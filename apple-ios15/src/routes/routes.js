import { Home } from "../components/home/home";
import { CardHome } from "../components/AppleCard/home/home";
import { Overview } from "../components/apple-ios15/overview/overview";
import { Features } from "../components/apple-ios15/features/features";
export const Routes = [
  {
    path: "/apple-ios13",
    exact: true,
    component: Home,
  },
  {
    path: "/",
    exact: true,
    component: Overview,
  },
  {
    path: "/features",
    exact: true,
    component: Features,
  },
  {
    path: "/apple-card",
    exact: true,
    component: CardHome,
  },
];
