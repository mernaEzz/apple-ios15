import React from "react";

import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Routes } from "./routes/routes";
function App() {
  return (
    <BrowserRouter>
      <Switch>
        {Routes.map((route, index) => (
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.component}
          />
        ))}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
