import React, { Component } from "react";
import { Links } from "../../../elements/links/links";

export class WebFooter extends Component {
  state = {
    content: [
      {
        title: "Explore",
        data: [
          { Link: "#", contentName: "Mac" },
          { Link: "#", contentName: "iPad" },
          { Link: "#", contentName: "iPhone" },
          { Link: "#", contentName: "Watch" },
          { Link: "#", contentName: "AirPods" },
          { Link: "#", contentName: "TV & Home" },
          { Link: "#", contentName: "iPod touch" },
          { Link: "#", contentName: "AirTag" },
        ],
      },
      {
        title: "Services",
        data: [
          { Link: "#", contentName: "Apple Music" },
          { Link: "#", contentName: "Apple TV+" },
          { Link: "#", contentName: "Apple Arcade " },
          { Link: "#", contentName: "iCloud" },
          { Link: "#", contentName: "Apple One" },
          { Link: "#", contentName: "Apple Books" },
          { Link: "#", contentName: "Apple Podcasts" },
          { Link: "#", contentName: "App Store" },
        ],
      },
      {
        title: "Account",
        data: [
          { Link: "#", contentName: "Manage Your Apple ID" },
          { Link: "#", contentName: "iCloud.com" },
        ],
      },
      {
        title: "About Apple",
        data: [
          { Link: "#", contentName: "Newsroom" },
          { Link: "#", contentName: "Apple Leadership" },
          { Link: "#", contentName: "Career Opportunities" },
          { Link: "#", contentName: "Investors" },
          { Link: "#", contentName: "Ethics & Compliance" },
        ],
      },
    ],
  };
  render() {
    let { content } = this.state;
    return (
      <div className="web-footer ">
        <div className="container-fluid">
          <div className="row">
            {content.map((content, id) => {
              return (
                <div className="col-3" key={id}>
                  <h4>{content.title}</h4>
                  <ul>
                    {content.data.map((content, contentId) => {
                      return (
                        <li key={contentId}>
                          <Links to={content.Link}>{content.contentName}</Links>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
