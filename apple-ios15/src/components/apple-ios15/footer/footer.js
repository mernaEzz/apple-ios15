import React from "react";
import { Links } from "../../elements/links/links";
import "./footer.css";
import { MobileFooter } from "./mobileFooter/mobileFooter";
import { WebFooter } from "./webFooter/webFooter";
export const Footer = () => {
  return (
    <footer className="footer gray-background">
      <div className="grid">
        <div className="content">
          <div className="footer-content">
            <ol className="gray-background">
              <li>
                <div className="footer-logo"></div>
              </li>
              <li>
                <Links to="#">iOS</Links>
              </li>
              <li>iOS 15</li>
            </ol>
            <WebFooter />
            <MobileFooter />
            <div className="bottom-footer">
              <p className="find-retailer">
                <Links to="#">Find a retailer</Links> near you.
              </p>
              <div className="row ">
                <div className="row copyright">
                  <p> Copyright &copy; 2021 Apple Inc. All rights reserved.</p>
                  <div className="links">
                    <ul>
                      <li>
                        <Links to="#">Privacy Policy </Links>
                      </li>
                      <li>
                        <Links to="#">Terms of Use</Links>
                      </li>
                      <li>
                        <Links to="#" className="site-map">
                          Site Map
                        </Links>
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="languages links">
                  <ul>
                    <li>
                      <Links to="#">English</Links>
                    </li>
                    <li>
                      <Links to="#" className="arabic">
                        عربى
                      </Links>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};
