import React, { Component, Fragment } from "react";
import { Links } from "../../../elements/links/links";

export class MobileFooter extends Component {
  state = {
    content: [
      {
        id: "explore",

        title: "Explore",
        links: [
          { Link: "#", contentName: "Mac" },
          { Link: "#", contentName: "iPad" },
          { Link: "#", contentName: "iPhone" },
          { Link: "#", contentName: "Watch" },
          { Link: "#", contentName: "AirPods" },
          { Link: "#", contentName: "TV & Home" },
          { Link: "#", contentName: "iPod touch" },
          { Link: "#", contentName: "AirTag" },
        ],
      },
      {
        id: "service",
        title: "Services",
        links: [
          { Link: "#", contentName: "Apple Music" },
          { Link: "#", contentName: "Apple TV+" },
          { Link: "#", contentName: "Apple Arcade " },
          { Link: "#", contentName: "iCloud" },
          { Link: "#", contentName: "Apple One" },
          { Link: "#", contentName: "Apple Books" },
          { Link: "#", contentName: "Apple Podcasts" },
          { Link: "#", contentName: "App Store" },
        ],
      },
      {
        id: "account",
        title: "Account",
        links: [
          { Link: "#", contentName: "Manage Your Apple ID" },
          { Link: "#", contentName: "iCloud.com" },
        ],
      },
      {
        id: "apple",
        title: "About Apple",
        links: [
          { Link: "#", contentName: "Newsroom" },
          { Link: "#", contentName: "Apple Leadership" },
          { Link: "#", contentName: "Career Opportunities" },
          { Link: "#", contentName: "Investors" },
          { Link: "#", contentName: "Ethics & Compliance" },
        ],
      },
    ],
  };
  handleClickedLink = (id) => {
    this.setState({ [id]: !this.state[id] });
  };
  render() {
    let { content } = this.state;
    return (
      <div className="mobile-footer">
        {content.map((data, id) => {
          return (
            <Fragment key={id}>
              <h4
                className={`${
                  this.state[data.id] ? "active-title" : "in-active-title"
                } gray-background`}
                onClick={() => this.handleClickedLink(data.id)}
              >
                {data.title}
              </h4>
              <ul
                className={`${
                  this.state[data.id] ? "active" : "in-active"
                } details-link`}
              >
                {" "}
                <div>
                  {data.links.map((name, dataId) => {
                    return (
                      <li>
                        <Links to="#" key={dataId}>
                          {name.contentName}
                        </Links>
                      </li>
                    );
                  })}{" "}
                </div>
              </ul>
            </Fragment>
          );
        })}
      </div>
    );
  }
}
