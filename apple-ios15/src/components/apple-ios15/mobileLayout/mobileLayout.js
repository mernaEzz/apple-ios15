import React from "react";
import "./mobileLayout.css";
export class MobileLayout extends React.PureComponent {
  render() {
    return (
      <>
        {" "}
        <div
          className={this.props.className}
          id="mobile-layout"
          style={this.props.style}
        >
          <div className="hardware"></div>
          <div className="screen" id={this.props.id}></div>
        </div>
      </>
    );
  }
}
