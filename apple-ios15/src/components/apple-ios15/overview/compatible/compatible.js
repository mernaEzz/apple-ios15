import React from "react";
import { Links } from "../../../elements/links/links";
import "./compatible.css";
export class Compatible extends React.PureComponent {
  state = {
    list: [
      { type: "iPhone 13" },
      { type: "iPhone 13 mini" },
      { type: "iPhone 13 Pro" },
      { type: "iPhone 13 Pro Max" },
      { type: "iPhone 12" },
      { type: "iPhone 12 mini" },
      { type: "iPhone 12 Pro" },
      { type: "iPhone 12 Pro Max" },
      { type: "iPhone 11" },
      { type: "iPhone 11 Pro" },
      { type: "iPhone 11 Pro Max" },
      { type: "iPhone Xs" },
      { type: "iPhone Xs Max" },
      { type: "iPhone Xr" },
      { type: "iPhone X" },
      { type: "iPhone 8" },
      { type: "iPhone 8 Plus" },
      { type: "iPhone 7" },
      { type: "iPhone 7 Plus" },
      { type: "iPhone 6s" },
      { type: "iPhone 6s Plus" },
      { type: "iPhone SE (1st generation)" },
      { type: "iPhone SE (2nd generation)" },
      { type: "iPod touch (7th generation)" },
    ],
  };
  render() {
    let { list } = this.state;
    return (
      <div className="compatible ">
        <div className="compatible-figure"></div>
        <div className="compatible-content  flex-content">
          <div className="large-6 left">
            <h5>iOS 15 is compatible with these devices.</h5>
            <div className="links">
              <Links to="#">
                Learn more about iPhone<i className="fa fa-angle-right	"></i>
              </Links>
              <Links to="#">
                Learn more about iPod touch
                <i className="fa fa-angle-right	"></i>
              </Links>
            </div>
          </div>
          <div className="types large-6">
            <ul>
              {list.map((data, id) => {
                return <li key={id}>{data.type}</li>;
              })}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
