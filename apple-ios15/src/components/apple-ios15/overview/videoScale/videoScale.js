import React, { Component } from "react";
import videoCollection from "../../../../assets/videos/photo-collection.mp4";
export class VideoScale extends Component {
  state = {
    videoScale: 1,
    playPaused: "",
    playPauseBtn: true,
    icon: "",
  };
  componentDidMount() {
    window.addEventListener("load", this.handlePlayVideo);
    window.addEventListener("scroll", this.handlePlayVideo);
  }
  handlePlayVideo = () => {
    let { videoScale, playPaused, icon } = this.state;
    icon = "";
    if (document.querySelectorAll("video[autoPlay]").length > 0) {
      var windowHeight = window.innerHeight,
        videoEl = document.querySelectorAll("video[autoPlay]");
      for (var i = 0; i < videoEl.length; i++) {
        var thisVideoEl = videoEl[i],
          videoHeight = thisVideoEl.clientHeight,
          videoClientRect = thisVideoEl.getBoundingClientRect().top;
        if (
          videoClientRect <= windowHeight - videoHeight * 1.19 &&
          videoClientRect >= 0 - videoHeight * 1.19
        ) {
          thisVideoEl.play();
          videoScale = 1.8;
          playPaused = "pause";
        } else if (
          videoClientRect >= -(windowHeight - videoHeight * 4) &&
          videoClientRect >= 9 - videoHeight * 0.4
        ) {
          thisVideoEl.pause();
          videoScale = 1;
        }
      }

      if (playPaused === "play") {
        icon = <i className="fa fa-play-circle-o"></i>;
      }
      if (playPaused === "pause") {
        icon = <i className="fa fa-pause-circle-o"></i>;
      }
      if (playPaused === "retry") {
        icon = <i className="fa fa-repeat"></i>;
      }
    }
    this.setState({ videoScale, playPaused, icon });
  };
  handlePlayPause = () => {
    let { playPauseBtn, playPaused, icon } = this.state;
    var video = document.getElementById("video");
    playPauseBtn === true ? video.pause() : video.play();
    playPauseBtn === true ? (playPaused = "play") : (playPaused = "pause");
    if (playPaused === "play") {
      icon = <i className="fa fa-play-circle-o"></i>;
    }
    if (playPaused === "pause") {
      icon = <i className="fa fa-pause-circle-o"></i>;
    }
    if (playPaused === "retry") {
      icon = <i className="fa fa-repeat"></i>;
    }
    this.setState({ playPauseBtn: !playPauseBtn, playPaused, icon });
  };
  handlevideoEnd = () => {
    let { playPauseBtn, playPaused, icon } = this.state;
    playPauseBtn = false;
    playPaused = "retry";
    if (playPaused === "retry") {
      icon = <i className="fa fa-repeat"></i>;
    }
    this.setState({ playPauseBtn, playPaused, icon });
  };
  render() {
    let { videoScale } = this.state;
    return (
      <>
        <p
          className="play-pause"
          style={{ opacity: videoScale === 1 ? "0" : "1" }}
          onClick={() => this.handlePlayPause()}
        >
          {this.state.playPaused}
          {this.state.icon}
        </p>
        <div
          className={`vertical xx ${
            videoScale === 1.8 ? "mobile-scaled" : "mobile-default"
          }`}
          id="mobile-layout"
        >
          {" "}
          <div className="hardware"></div>
          <div className="screen">
            {this.props.name === "photo-collection" && (
              <video
                autoPlay
                onEnded={this.handlevideoEnd}
                id="video"
                src={videoCollection}
              />
            )}
            {this.props.name === "notification-summary" && (
              <img
                src="https://www.apple.com/v/ios/ios-15/b/images/overview/in-the-moment/notifications_summary__e5r298lvwhiu_large.jpg"
                alt="summary"
              />
            )}
          </div>
        </div>
      </>
    );
  }
}
