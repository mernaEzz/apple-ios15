import React from "react";
import signalImg from "../../../../assets/images/signal.jpg";
import signalSmall from "../../../../assets/images/signal-small.jpg";
import { MobileLayout } from "../../mobileLayout/mobileLayout";
import "./focus.css";
export const Focus = () => {
  return (
    <div className="focus gray-background">
      <div className="grid">
        <div className="content ">
          <h1>Focus</h1>
          <div className="focus-content ">
            <div className="match-device grid-6 white-background">
              <h2>Match your devices to your mindset</h2>
              <p>
                Focus helps you stay in the moment when you need to concentrate
                or step away. Choose from a list of Focus options or create your
                own to allow only the notifications you want — you can get work
                done while you’re in the zone or enjoy a distraction‑free
                dinner.
              </p>
              <MobileLayout className="vertical" id="focus-on" />
            </div>
            <div className="signal-status grid-6 white-background">
              <h2>Signal your status</h2>
              <p>
                Stepping away is easier if others know you’re busy. So when
                you’re using Focus, your status will be automatically displayed
                in Messages and other communication apps you allow. And for
                truly urgent messages, there’s still a way for people to notify
                you.
              </p>
              <img src={signalImg} alt="chats" className="signal-img" />
              <img
                src={signalSmall}
                alt="signal"
                className="signal-small-img"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
