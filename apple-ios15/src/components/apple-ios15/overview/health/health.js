import React from "react";
import { HealthSpotLayout } from "../healthSpotLayout/healthSpotLayout";
import healthImg from "../../../../assets/images/health.jpg";
import healthMobileImg from "../../../../assets/images/health-mobile.jpg";
import "./health.css";
export const Health = () => {
  return (
    <HealthSpotLayout
      id="health"
      name="health"
      h1="Health"
      h2="Health app updates"
      text="Updates to the Health app give you new ways to share data with your loved ones, a metric to assess your risk of falling, and trend analysis to help you understand changes in your health."
      src={healthImg}
      mobileImgSrc={healthMobileImg}
      alt="spotlight-img"
    />
  );
};
