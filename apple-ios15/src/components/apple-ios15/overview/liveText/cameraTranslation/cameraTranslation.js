import React from "react";
import { MobileLayout } from "../../../mobileLayout/mobileLayout";
import "./cameraTranslation.css";
export const CameraTranslation = () => {
  return (
    <div className="focus camera-translation gray-background">
      <div className="grid">
        <div className="content ">
          <div className="focus-content ">
            <div className="live-text-camera  grid-6 white-background">
              <h2>Live Text in Camera</h2>
              <p>
                Live Text also works in the Camera app, so you can point your
                iPhone camera at text on the go and quickly take action on
                useful information.
              </p>
            </div>
            <div className="signal-status grid-6 white-background">
              <h2>Live Text translation</h2>
              <p>
                Live Text understands seven different languages: English,
                Chinese, French, Italian, German, Portuguese, and Spanish. And
                with system‑wide translation, you can simply tap and translate.
              </p>{" "}
              <MobileLayout className="vertical" id="translation" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
