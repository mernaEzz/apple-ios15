import React from "react";
import { OverlayContent } from "../../overlayContent/overlayContent";
import liveImg from "../../../../assets/images/live-text.jpg";
import liveMobileImg from "../../../../assets/images/live-text-mobile.jpg";
import "./liveText.css";
import { CameraTranslation } from "./cameraTranslation/cameraTranslation";
export const LiveText = () => {
  return (
    <>
      <div className="live-text gray-background">
        <div className="grid">
          <div className="content ">
            <h1>Live Text</h1>
            <div className="live-text-content grid-12 white-background">
              <OverlayContent
                h2="Live Text in photos"
                name="live-text"
                p="Live Text intelligently unlocks rich and useful information in images, so you can make a call, send an email, or look up directions with just a tap on the highlighted text in a photo."
                src={liveImg}
                mobileSrc={liveMobileImg}
                overlayP="With Live Text, you can use familiar text selection gestures to highlight, copy and paste, look up, and even translate text in live previews with Camera or in Photos, Screenshot, Quick Look, and Safari."
              ></OverlayContent>
            </div>
          </div>
        </div>
      </div>
      <CameraTranslation />
    </>
  );
};
