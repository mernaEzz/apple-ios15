import React from "react";
import spatialWebImg from "../../../../assets/images/spatial-web.jpg";
import spatialsmallbImg from "../../../../assets/images/spatial-small.jpg";
import "./spatialAudio.css";
export const SpatialAudio = () => {
  return (
    <div className="spatial-audio gray-background">
      <div className="grid">
        <div className="content ">
          <div className="share-play white-background">
            <div className="content-data ">
              <h2>Spatial audio</h2>
              <p>
                Individual voices sound like they’re coming from the direction
                in which each person is positioned on your screen, helping
                conversations flow more naturally.
                <sup>
                  <a href="#two">2</a>
                </sup>
              </p>
            </div>
            <img className="web-img" src={spatialWebImg} alt="screen" />
            <img className="mobile-img" src={spatialsmallbImg} alt="mobile" />
          </div>
        </div>
      </div>
    </div>
  );
};
