import React from "react";
import { OverlayContent } from "../../overlayContent/overlayContent";
import interactiveImg from "../../../../assets/images/interactive.jpg";
import musicImg from "../../../../assets/images/music.png";
import "./photos.css";
export const Photos = () => {
  return (
    <>
      <div className="photos gray-background">
        <div className="grid">
          <div className="content ">
            <h1>Photos</h1>
            <div className="photos-content">
              <div className=" grid-6  white-background interactive ">
                <h2>Interactive Memories with a new look</h2>
                <p name=" links">
                  Memories introduces a new interactive, immersive interface,
                  along with new Memory mixes that let you personalize the look
                  and feel of your story with a song and vibe to match.
                </p>
                <div className="row">
                  <img src={interactiveImg} alt="interactive-img" />
                </div>
              </div>
              <div
                style={{ position: "relative" }}
                className=" grid-6 white-background apple-music "
              >
                <OverlayContent
                  h2="Apple Music + Memories"
                  name="apple-music"
                  h3="Tens of millions of songs in the Apple Music library. Now available in Memories."
                  imgSrc={musicImg}
                  alt="music-icon"
                  overlayP="Experts have curated sets of songs that are combined with your music tastes and listening history, and the knowledge of what’s in your photos and videos, to create personalized recommendations just for you. Or if you have the perfect jam in mind, you can choose your own song.6"
                ></OverlayContent>
              </div>{" "}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
