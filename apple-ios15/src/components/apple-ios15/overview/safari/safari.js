import React, { Component } from "react";
import { VideoAutoPlay } from "../videoAutoPlay/videoAutoPlay";
import { MobileLayout } from "../../mobileLayout/mobileLayout";
import { OverlayContent } from "../../overlayContent/overlayContent";
import extensionImg from "../../../../assets/images/extension.jpg";
import "./safari.css";
export class Safari extends Component {
  state = {
    data: [
      {
        class: "bottom-tab",
        title: "Bottom tab bar",
        content:
          "The bottom tab bar puts controls right at your fingertips. Swipe left or right on the address bar to move between tabs. Or swipe up to see all your open tabs.",
      },
      {
        class: "tab-group",
        title: "Tab Groups",
        content:
          "Save and organize your tabs in the way that works best for you and switch between them easily. Tab Groups sync across devices so you have access to your tabs from anywhere.",
        img: <MobileLayout className="vertical" id="tab-img" />,
      },
    ],
  };
  render() {
    let { data } = this.state;
    return (
      <div className="safari gray-background">
        <div className="grid">
          <div className="content ">
            <h1>Safari</h1>
            <div className="safari-content ">
              {data.map((contentData, id) => {
                return (
                  <div
                    key={id}
                    className={`${contentData.class} grid-6 white-background`}
                  >
                    <h2>{contentData.title}</h2>
                    <p>{contentData.content}</p>
                    {contentData.title === "Bottom tab bar" ? (
                      <VideoAutoPlay name="safari-video" />
                    ) : (
                      <div className="img-container">{contentData.img}</div>
                    )}
                  </div>
                );
              })}
              <div
                style={{ position: "relative" }}
                className="face-time-links grid-4 white-background"
              >
                <OverlayContent
                  h2="Voice search"
                  name="voice-search"
                  h3="Search the web using just your voice."
                  overlayP="Voice search in Safari is an incredibly convenient way to search the web hands‑free. Simply tap the microphone in the search field and speak."
                ></OverlayContent>
              </div>
              <div className="invite-facetime grid-8 white-background">
                <h2>Extensions</h2>
                <p name=" links">
                  Now you can install Safari extensions on your iPhone. And just
                  like on Mac, you can choose when the extensions will be
                  active.
                </p>
                <img src={extensionImg} alt="extension" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
