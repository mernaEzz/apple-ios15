import React from "react";
import { MobileLayout } from "../../mobileLayout/mobileLayout";
import "./healthSpotLayout.css";
export const HealthSpotLayout = (props) => {
  return (
    <div className="live-text health-spotlight  gray-background" id={props.id}>
      <div className="grid">
        <div className="content ">
          <h1>{props.h1}</h1>
          <div className="live-text-content grid-12 white-background">
            <h2>{props.h2}</h2>
            <p>{props.text}</p>
            <img src={props.src} alt={props.alt} className="web-img" />
            {props.name === "spotlight" && (
              <MobileLayout className="vertical" id="spotlight-mobile-img" />
            )}
            {props.name === "health" && (
              <img src={props.mobileImgSrc} id="health-mobile-img" alt="img" />
            )}
            {props.name === "face-time" && (
              <img
                src={props.mobileImgSrc}
                id="facetime-mobile-img"
                alt="img"
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
