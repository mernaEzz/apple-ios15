import React from "react";
import "./icloud.css";
import { IcloudContent } from "./icloudContent/icloudContent";
export const Icloud = () => {
  return (
    <div className="icloud gray-background ">
      <div className="grid">
        {" "}
        <div className="content">
          <div className="title-div">
            {" "}
            <h1>iCloud+</h1>{" "}
            <p>
              iCloud has always kept your important information — like photos,
              documents, and notes — safe, up to date, and accessible across all
              your devices. Now iCloud+ takes that experience a step further,
              with an all-new subscription that will replace today’s storage
              plans.
              <sup>
                <a href="#eight">8</a>
              </sup>
            </p>
          </div>{" "}
          <IcloudContent />
        </div>
      </div>
    </div>
  );
};
