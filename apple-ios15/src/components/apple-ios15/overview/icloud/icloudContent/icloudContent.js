import React from "react";
import { MobileLayout } from "../../../mobileLayout/mobileLayout";
import icloudImg from "../../../../../assets/images/icloud-section.jpg";
export const IcloudContent = () => {
  return (
    <div className="icloud-content ">
      <div className="match-device grid-6 white-background">
        <h2>Introducing iCloud+</h2>
        <p>
          Get everything you already love about iCloud, along with new features
          like Hide My Email.
        </p>
        <div className="img-container">
          <img src={icloudImg} alt="icloud-img" />
        </div>
      </div>
      <div className="signal-status grid-6 white-background">
        <h2>Hide My Email</h2>
        <p>
          Instantly generate unique, random email addresses that forward to your
          personal inbox — so you don’t have to share your real email address
          when filling out a form on the web or signing up for a newsletter.
          Hide My Email is built into Mail, Safari, and iCloud Settings.
        </p>
        <MobileLayout className="vertical" id="hide-email" />
      </div>
    </div>
  );
};
