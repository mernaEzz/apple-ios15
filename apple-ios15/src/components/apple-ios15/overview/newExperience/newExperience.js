import React from "react";
export const NewExperience = () => {
  return (
    <div className="new-experience gray-background title-div">
      <div className="grid">
        {" "}
        <div className="content">
          <h1>
            New experiences.
            <br /> True connections.
          </h1>
          <p>
            Staying in touch is more essential than ever. With SharePlay, you
            can watch together, listen together, and share your screen right
            inside FaceTime.
            <sup>
              <a href="#one" className="num">
                1
              </a>
            </sup>{" "}
            FaceTime calls also sound and feel more natural. And Messages makes
            it even easier to find and enjoy the great content shared from
            friends and family.
          </p>
        </div>
      </div>
    </div>
  );
};
