import React from "react";
import spotlightImg from "../../../../assets/images/spotlight.jpg";
import { HealthSpotLayout } from "../healthSpotLayout/healthSpotLayout";
import "./spotlight.css";
export const Spotlight = () => {
  return (
    <HealthSpotLayout
      id="spotlight"
      name="spotlight"
      h1="Spotlight"
      h2="Rich results and photos search"
      text=" Spotlight shows you more information at a glance with new rich search results for your contacts. And you can now search your
            photos in Spotlight, and even search based on text in your photos
              by using Live Text."
      src={spotlightImg}
      alt="spotlight-img"
    />
  );
};
