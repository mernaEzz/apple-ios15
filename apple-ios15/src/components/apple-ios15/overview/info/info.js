import React from "react";
import "./info.css";
export class Info extends React.PureComponent {
  state = {
    info: [
      {
        class: "one",
        content:
          "Users with non-Apple devices can join using the latest version of Chrome or Edge. Sending video requires H.264 video encoding support. Some shared content may require a subscription to view.",
      },
      {
        class: "two",
        content: "Available on iPhone with A12 Bionic and later.          ",
      },
      {
        class: "three",
        content:
          "Available on iPhone with A12 Bionic and later. CarPlay support coming in a software update later this year.          ",
      },
      {
        class: "four",
        content: "Available late 2021.          ",
      },
      {
        class: "five",
        content:
          "Available on iPhone with A12 Bionic and later. Live Text is currently supported in English, Chinese, French, Italian, German, Portuguese, and Spanish.          ",
      },
      {
        class: "six",
        content: "Requires an Apple Music subscription.          ",
      },
      {
        class: "seven",
        content: "Coming in a software update to iOS 15.        ",
      },
      {
        class: "eight",
        content:
          "iCloud+ plans: 50GB with one HomeKit Secure Video camera ($0.99/mo.), 200GB with up to five HomeKit Secure Video cameras ($2.99/mo.), and 2TB with an unlimited number of HomeKit Secure Video cameras ($9.99/mo.). ",
      },
    ],
  };
  render() {
    let { info } = this.state;
    return (
      <div className="info gray-background">
        <div className="grid">
          <div className="content">
            <div className="info-content">
              <ul>
                {info.map((data, id) => {
                  return (
                    <li id={data.class} key={id}>
                      {data.content}
                    </li>
                  );
                })}
              </ul>
              <p>
                Features are subject to change. Some features, applications, and
                services may not be available in all regions or all languages.
              </p>
              <p>
                Text contained in Billie Eilish Wikipedia entries is available
                under the Creative Commons Attribution-ShareAlike License at
                http://creativecommons.org/licenses/by-sa/3.0. Wikipedia® is a
                registered trademark of the Wikimedia Foundation, Inc., a
                nonprofit organization.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
