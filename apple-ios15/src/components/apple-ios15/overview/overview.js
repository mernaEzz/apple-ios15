import React, { Component } from "react";
import { Navbar } from "../../navbar/navbar";
import { Ios15Nav } from "../ios15Nav/ios15Nav";
import { Header } from "../overview/header/header";
import { FaceTime } from "./faceTime/faceTime";
import { GridPortraitMic } from "./gridPortraitMic/gridPortraitMic";
import { Messages } from "./messages/messages";
import { NewExperience } from "./newExperience/newExperience";
import { SpatialAudio } from "./spatialAudio/spatialAudio";
import { WatchListenShare } from "./watchListenShare/watchListenShare";
import "./overview.css";
import { BeInMoment } from "./beInMoment/beInMoment";
import { Focus } from "./focus/focus";
import { Notifications } from "./notifications/notifications";
import { BrandNewWays } from "./brandNewWays/brandNewWays";
import { Maps } from "./maps/maps";
import { Safari } from "./safari/safari";
import { Intelligence } from "./intelligence/intelligence";
import { LiveText } from "./liveText/liveText";
import { Spotlight } from "./spotlight/spotlight";
import { Health } from "./health/health";
import { Icloud } from "./icloud/icloud";
import { Photos } from "./photos/photos";
import { Privacy } from "./privacy/privacy";
import { SoMuch } from "./soMuch/soMuch";
import { Compatible } from "./compatible/compatible";
import { UpdateSdk } from "./updateSdk/updateSdk";
import { Ipados } from "./ipados/ipados";
import { Info } from "./info/info";
import { Footer } from "../footer/footer";
import { Helmet, HelmetProvider } from "react-helmet-async";

export class Overview extends Component {
  state = {
    scroll: false,
    count: -500,
  };
  componentDidMount() {
    window.scrollTo(0, 0);

    window.onscroll = () => {
      if (window.scrollY >= 50) {
        this.handlePageScroll();
      } else {
        this.setState({
          scroll: false,
        });
      }
    };
  }
  handlePageScroll = () => {
    this.setState({
      scroll: true,
    });
  };
  render() {
    return (
      <>
        {" "}
        <HelmetProvider>
          <Helmet>
            <title>iOS 15 - Apple(EG)</title>
          </Helmet>
        </HelmetProvider>
        <div className="home-page">
          <Navbar />
          <Ios15Nav scroll={this.state.scroll} />
          <Header />
          <NewExperience />{" "}
          <div className="overview">
            <FaceTime />
            <WatchListenShare />
            <SpatialAudio />
            <GridPortraitMic />
            <Messages />
            <BeInMoment />
            <Focus />
            <Notifications />
            <BrandNewWays />
            <Maps />
            <Safari />
            <Intelligence />
            <LiveText />
            <Spotlight />
            <Photos />
            <Health />
            <Privacy />
            <Icloud />
            <SoMuch />
            <Compatible />
            <UpdateSdk />
            <Ipados />
            <Info />
          </div>{" "}
          <Footer />
        </div>
      </>
    );
  }
}
