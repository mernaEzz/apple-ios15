import React, { Component } from "react";
import shareWithImg from "../../../../assets/images/share-with-you.jpg";
import shareAppImg from "../../../../assets/images/share-app.jpg";
import newMemoji from "../../../../assets/images/new-memoji.jpg";
import { MobileLayout } from "../../mobileLayout/mobileLayout";
import { VideoScale } from "../videoScale/videoScale";
import "./messages.css";
export class Messages extends Component {
  state = {
    data: [
      {
        class: "share-with-you white-background grid-8",
        title: "Shared with You",
        content:
          "Now the links, images, and other content shared with you in Messages are featured in a new Shared with You section in the corresponding app. You can even reply right from the app you’re enjoying it from — without going back to Messages.",
        img: <img src={shareWithImg} alt="share-mobile" />,
      },
      {
        class: "share-app white-background grid-4",
        title: "Shared with You in apps",
        content:
          "Shared with You is built into Photos, Safari, Apple Music, Apple Podcasts, and the Apple TV app.",
        img: <img src={shareAppImg} alt="share-app" />,
      },
      {
        class: "new-memoji white-background grid-4",
        title: "New Memoji",
        content:
          "Now you can choose outfits for your Memoji and express yourself with new stickers. Represent your look and style with multicolor headwear. And accessibility customizations now include cochlear implants, oxygen tubes, and soft helmets.",
        img: <img src={newMemoji} alt="share-app" />,
      },
      {
        class: "photo-collections white-background  grid-8",
        title: "Photo collections",
        content:
          "Multiple photos in Messages now appear as a collage or an elegant stack of images that you can swipe through. Tap to view them all as a grid or add a quick Tapback. And save them to your library with just a few taps.",
        img: <MobileLayout className="vertical" id="collections" />,
      },
    ],
  };

  render() {
    let { data } = this.state;
    return (
      <div className="messages-memoji gray-background">
        <div className="grid">
          <div className="content ">
            <h1>Messages and Memoji</h1>
            <div className="content-data">
              {data.map((data, id) => {
                return (
                  <div className={data.class} key={id}>
                    <h2>{data.title}</h2>
                    <p>{data.content}</p>

                    {data.title === "Photo collections" ? (
                      <>
                        <VideoScale name="photo-collection" id="video" />
                      </>
                    ) : (
                      <div className="img-container">{data.img}</div>
                    )}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
