import React, { Component } from "react";
import { MobileLayout } from "../../mobileLayout/mobileLayout";
export class ImgScale extends Component {
  state = {
    imgScale: 1,
  };
  componentDidMount() {
    window.addEventListener("load", this.handleImgScale);
    window.addEventListener("scroll", this.handleImgScale);
    console.log(this.props.name);
  }
  componentWillUnmount() {
    window.removeEventListener("load", this.handleImgScale);
    window.removeEventListener("scroll", this.handleImgScale);
  }
  handleImgScale = (e) => {
    let { imgScale } = this.state;
    let scrollPosition = window.scrollY;
    var imgScaleDiv = document.getElementsByClassName("img-scale");
    var offsetTop = imgScaleDiv[0].offsetTop;
    if (
      scrollPosition >=
      offsetTop + imgScaleDiv[0].clientHeight * 4.95 - window.innerHeight
    ) {
      imgScale = 1.8;
    } else if (
      scrollPosition <=
      offsetTop + imgScaleDiv[0].clientHeight * 4 - window.innerHeight
    ) {
      imgScale = 1;
    }
    this.setState({ imgScale });
  };
  render() {
    let { imgScale } = this.state;
    return (
      <>
        {this.props.name === "notification-summary" && (
          <MobileLayout
            className={`vertical xx img-scale ${
              imgScale === 1.8 ? "img-scaled" : "img-default"
            }`}
            id={this.props.id}
            // style={{
            //   transform:
            //     " matrix(" + imgScale + ", 0, 0," + imgScale + ", 0, 0)",
            // }}
          />
        )}
      </>
    );
  }
}
