import React, { Component } from "react";
import immersiveVideo from "../../../../assets/videos/immersive.mp4";
export class VideoAutoPlay extends Component {
  state = {
    playPaused: "",
    playPauseBtn: true,
  };
  componentDidMount() {
    window.addEventListener("load", this.handleAutoPlay);
    window.addEventListener("scroll", this.handleAutoPlay);
  }
  componentWillUnmount() {
    window.removeEventListener("load", this.handleAutoPlay);
    window.removeEventListener("scroll", this.handleAutoPlay);
  }
  handleAutoPlay = () => {
    let { playPaused } = this.state;
    if (this.props.name === "maps") {
      var videoDiv = document.getElementsByClassName("map-video");
      var video = document.getElementById("video2");
    }
    if (this.props.name === "safari-video") {
      videoDiv = document.getElementsByClassName("safari-video-div");
      video = document.getElementById("video-safari");
    }
    // if (this.props.name === "maps"
    let scrollPosition = window.scrollY;
    var offsetTop = videoDiv[0].offsetTop;

    if (
      scrollPosition >=
      offsetTop + videoDiv[0].clientHeight - window.innerHeight
    ) {
      video.play();
      playPaused = "pause";
    } else if (
      scrollPosition <=
      offsetTop + videoDiv[0].clientHeight * -0.78 - window.innerHeight
    ) {
      video.pause();
      playPaused = "play";
    }

    this.setState({
      playPaused,
    });
  };

  handleClickedBtn = () => {
    let { playPaused, playPauseBtn } = this.state;
    if (this.props.name === "maps") {
      var video = document.getElementById("video2");
    }
    if (this.props.name === "safari-video") {
      video = document.getElementById("video-safari");
    }
    if (playPauseBtn === true) {
      video.pause();
      playPaused = "play";
    } else {
      video.play();
      playPaused = "pause";
    }
    this.setState({
      playPaused,
      playPauseBtn: !playPauseBtn,
    });
  };

  handlevideoEnd = () => {
    let { playPauseBtn, playPaused } = this.state;
    playPauseBtn = false;
    playPaused = "retry";
    this.setState({ playPauseBtn, playPaused });
  };

  render() {
    let { playPaused } = this.state;

    return (
      <>
        <div
          className="vertical xx  map-video safari-video-div"
          id="mobile-layout"
        >
          {" "}
          <div className="hardware"></div>
          <div className="screen">
            {this.props.name === "maps" && (
              <video
                onEnded={this.handlevideoEnd}
                id="video2"
                src={immersiveVideo}
              />
            )}
            {this.props.name === "safari-video" && (
              <video
                onEnded={this.handlevideoEnd}
                id="video-safari"
                src="https://www.apple.com/105/media/ae/ios/ios-15/2021/18f8d1cf-c0d5-4f10-b1ff-15175e0e7a38/anim/safari/large_2x.mp4"
              />
            )}
          </div>
        </div>
        <p className="play-pause" onClick={this.handleClickedBtn}>
          {playPaused}
          {playPaused === "pause" && <i className="fa fa-pause-circle-o"></i>}
          {playPaused === "play" && <i className="fa fa-play-circle-o"></i>}
          {playPaused === "retry" && <i className="fa fa-repeat"></i>}
        </p>
      </>
    );
  }
}
