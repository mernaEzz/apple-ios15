import React from "react";
// import updateImg from "../../../../assets/images/update.jpg";
// import sdkImg from "../../../../assets/images/sdk.jpg";
import { Links } from "../../../elements/links/links";
import "./updateSdk.css";
export class UpdateSdk extends React.PureComponent {
  state = {
    content: [
      {
        class: "update",
        // img: updateImg,
        title: "See how to update to iOS 15.",
        link: "Learn more",
      },
      {
        class: "sdk",
        // img: sdkImg,
        title: "Developers will love iOS, too.",
        link: "Learn more about developing for iOS",
      },
    ],
  };
  render() {
    let { content } = this.state;
    return (
      <div className="update-sdk gray-background">
        <div className="grid">
          <div className="upate-sdk-content content">
            {content.map((data, id) => {
              return (
                <div
                  className={`${data.class} white-background grid-6`}
                  key={id}
                >
                  <div className="figure"></div>
                  <div className="data">
                    {" "}
                    <h5>{data.title}</h5>
                    <Links to="#">
                      {data.link}
                      {data.class === "update" ? (
                        <i className="fa fa-angle-right"></i>
                      ) : (
                        <i className="fa fa-external-link"></i>
                      )}
                    </Links>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}
