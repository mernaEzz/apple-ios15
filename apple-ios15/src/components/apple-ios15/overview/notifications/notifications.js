import React from "react";
import redesignImg from "../../../../assets/images/redesign.png";
import { ImgScale } from "../imgScale/imgScale";
import "./notifications.css";
export const Notifications = () => {
  return (
    <div className="notifications gray-background">
      <div className="grid">
        <div className="content ">
          <h1>Notifications</h1>
          <div className="content-data">
            <div className="redesigned grid-4 white-background">
              <h2>Redesigned notifications</h2>
              <p>
                Notifications have a new look, including contact photos and
                larger app icons to make them easier to identify.
              </p>
              <div className="img-container">
                <img src={redesignImg} alt="redesign-img" />
              </div>
            </div>
            <div className="notification-summary grid-8 white-background">
              <h2>Notification summary</h2>
              <p>
                Quickly catch up with a helpful collection of your notifications
                delivered daily, based on a schedule you set. The summary is
                intelligently ordered by priority, with the most relevant
                notifications at the top.
              </p>
              <ImgScale name="notification-summary" id="summary" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
