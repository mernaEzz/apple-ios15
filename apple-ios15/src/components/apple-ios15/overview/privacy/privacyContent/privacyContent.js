import React from "react";
import { MobileLayout } from "../../../mobileLayout/mobileLayout";
import { OverlayContent } from "../../../overlayContent/overlayContent";
import mailImg from "../../../../../assets/images/mail.png";
export const PrivacyContent = () => {
  return (
    <div className="privacy-content">
      <div className=" grid-6  white-background interactive ">
        <h2>App Privacy Report</h2>
        <p name=" links">
          See how apps are using the permissions you’ve granted them, which
          domains they contact,
          <sup>
            <a href="#seven">7</a>
          </sup>{" "}
          and how recently they made contact.
        </p>
        <MobileLayout className="vertical" id="app-privacy" />
      </div>
      <div
        style={{ position: "relative" }}
        className=" grid-6 white-background mail "
      >
        <OverlayContent
          h2="Mail Privacy Protection"
          name="privacy"
          h3="Catch up on email with more peace of mind."
          imgSrc={mailImg}
          alt="mail-icon"
          overlayP="Mail Privacy Protection hides your IP address, so senders can’t link it to your other online activity or determine your location. And it prevents senders from seeing if and when you’ve opened their email."
        ></OverlayContent>
      </div>{" "}
    </div>
  );
};
