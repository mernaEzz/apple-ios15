import React from "react";
import "./privacy.css";
import { PrivacyContent } from "./privacyContent/privacyContent";
export const Privacy = () => {
  return (
    <div className="privacy gray-background ">
      <div className="grid">
        {" "}
        <div className="content">
          <div className="title-div">
            {" "}
            <h1>Privacy </h1>{" "}
            <p>
              Great features should not have to come at the expense of your
              privacy. iOS 15 provides increased visibility into how apps access
              your data, protects you from unwanted data collection, and gives
              you more control over what you choose to share.
            </p>
          </div>

          <PrivacyContent />
        </div>
      </div>
    </div>
  );
};
