import React from "react";
export const BeInMoment = () => {
  return (
    <div className="be-in-moment gray-background title-div">
      <div className="grid">
        {" "}
        <div className="content">
          <h1>Be in the moment.</h1>
          <p>
            A powerful new set of tools gives you more control over how you
            prioritize your time and attention. So you can find balance and stay
            focused on whatever you’re doing in the moment.
          </p>
        </div>
      </div>
    </div>
  );
};
