import React, { Component } from "react";
import transitImg from "../../../../assets/images/transit.jpg";
import { MobileLayout } from "../../mobileLayout/mobileLayout";
import { VideoAutoPlay } from "../videoAutoPlay/videoAutoPlay";
import "./maps.css";
export class Maps extends Component {
  state = {
    playPaused: "",
    playPauseBtn: true,
    videoId: "video2",
    videoParentClass: "map-video",
    data: [
      {
        class: "all-new-city white-background grid-8",
        title: "All-new city experience",
        content:
          "Explore cities with unprecedented detail for roads, neighborhoods, trees, buildings, and more. Visit amazing 3D landmarks like the Golden Gate Bridge in both the day and dark mode maps.",
        sup: 3,
      },
      {
        class: "new-driving white-background grid-4",
        title: "New driving features",
        content:
          "Maps now offers drivers incredible road details like turn lanes, crosswalks, and bike lanes; street-level perspectives as you approach complex interchanges; and a new dedicated driving map that helps you see current incidents and traffic conditions at a glance.",
        img: <MobileLayout className="vertical" id="new-drive" />,
      },
      {
        class: "immersive white-background grid-4",
        title: "Immersive walking instructions",
        content:
          "Get where you’re going with step-by-step directions you can view in augmented reality.",
        sup: 2,
        img: <MobileLayout className="vertical" id="immersive-walking" />,
      },
      {
        class: "new-transit white-background grid-8",
        title: "New transit features",
        content:
          "Public transit integration shows nearby stations and transit times and lets you pin favorite routes to the top. And when you’re approaching your stop, Maps notifies you that it’s almost time to disembark.",
        img: <img src={transitImg} alt="share-mobile" />,
      },
    ],
  };

  render() {
    let { data } = this.state;
    return (
      <div className="maps gray-background">
        <div className="grid">
          <div className="content  ">
            <h1>Maps</h1>
            <div className="content-data">
              {data.map((data, id) => {
                return (
                  <div className={data.class} key={id}>
                    <h2>{data.title}</h2>
                    <p>
                      {data.content}
                      <sup>
                        <a href="#two">{data.sup}</a>
                      </sup>
                    </p>
                    {data.title === "Immersive walking instructions" ? (
                      <div className="title-body">
                        <div className="flex-content">
                          <VideoAutoPlay name="maps" />
                        </div>
                      </div>
                    ) : (
                      <div className="title-body">
                        <div className="img-container">{data.img}</div>
                      </div>
                    )}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
