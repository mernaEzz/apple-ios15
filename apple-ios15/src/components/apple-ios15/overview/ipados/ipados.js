import React from "react";
import { Links } from "../../../elements/links/links";
import "./ipados.css";
export const Ipados = () => {
  return (
    <div className="ipad-os gray-background">
      <div className="grid">
        <div className="content">
          <div className="ipad-os-content white-background">
            <div className="data">
              {" "}
              <h5>iPadOS 15</h5>
              <span>Work wonders. With ease.</span>
              <Links to="#">
                Learn more <i className="fa fa-angle-right"></i>
              </Links>
            </div>
            <div className="figure"></div>
          </div>
        </div>
      </div>
    </div>
  );
};
