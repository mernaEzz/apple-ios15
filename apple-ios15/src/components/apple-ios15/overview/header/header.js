import React, { Component } from "react";
import { MobileLayout } from "../../mobileLayout/mobileLayout";
import "./header.css";
export class Header extends Component {
  state = {
    transform: false,
  };
  componentDidMount() {
    setTimeout(() => {
      this.setState({ transform: true });
    }, 2000);
  }

  render() {
    let { transform } = this.state;
    return (
      <div className="ios15-overview-header">
        <img
          className="ios15-img"
          src="https://www.apple.com/v/ios/ios-15/b/images/overview/hero/icon_ios15__dx1nad135oia_large_2x.png"
          alt="ios15"
          style={{ transform: transform ? "scale(1)" : "scale(1.8)" }}
        />
        {transform && (
          <>
            <MobileLayout className="horizontal notes-div" id="notes" />
            <MobileLayout className="vertical home-div" id="home" />
            <MobileLayout
              className="vertical map-div"
              id="map"
              // style={{
              //   transform: "matrix(1, 0, 0, 1, 0, " + count + ")",
              // }}
            />
            <MobileLayout className="horizontal mixes-div" id="mixes" />
            <MobileLayout
              className="vertical notifications-div"
              id="notifications"
            />
            <MobileLayout className="vertical icons-div" id="icons" />
            <MobileLayout className="vertical disturb-div" id="disturb" />
            <MobileLayout className="vertical face-time-div" id="face-time" />
            <MobileLayout className="vertical faces-div" id="faces" />
            <MobileLayout className="vertical background-div" id="background" />
            <MobileLayout className="horizontal sport-div" id="sport" />
            <MobileLayout className="vertical chat-div" id="chat" />
            <MobileLayout className="vertical weather-div" id="weather" />
            <MobileLayout className="vertical news-div" id="news" />
            <MobileLayout className="horizontal cartoon-div" id="cartoon" />
            <MobileLayout className="vertical icloud-div" id="icloud" />
            <MobileLayout className="horizontal sea-div" id="sea" />
            <MobileLayout className="horizontal smile-div" id="smile" />
            <MobileLayout className="vertical focus-div" id="focus" />
            <div className="touch">
              <span>iOS15</span>
              <h1>
                In touch.
                <br />
                In the moment.
              </h1>
              <p>
                iOS 15 is packed with new features that help you connect with
                others, be more present and in the moment, explore the world,
                and use powerful intelligence to do more with iPhone than ever
                before.
              </p>
            </div>
          </>
        )}
      </div>
    );
  }
}
