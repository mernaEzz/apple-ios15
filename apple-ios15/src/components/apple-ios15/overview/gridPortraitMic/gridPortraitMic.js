import React from "react";
import { AudioIcon } from "../../../elements/audioIcon/audioIcon";
import { MobileLayout } from "../../mobileLayout/mobileLayout";
import { OverlayContent } from "../../overlayContent/overlayContent";
import { FaceTimeLinks } from "./faceTimeLinks/faceTimeLinks";
import "./gridPortraitMic.css";
export const GridPortraitMic = () => {
  return (
    <div className="  gray-background">
      <div className="grid">
        <div className="content  grid-portrait-mic">
          <div className="grid-view grid-4  white-background">
            <h2>Grid view</h2>
            <p>
              Grid view shows people on your FaceTime call in the same-size
              tiles, so you can have better conversations with a large group.
              The speaker is automatically highlighted so you always know who’s
              talking.
            </p>
            <div className="flex-content">
              <MobileLayout
                className="vertical"
                id="grid-view-mobile"
              ></MobileLayout>
            </div>
          </div>
          <div className="portrait-mode  grid-4 white-background">
            <h2>Portrait mode</h2>
            <p>
              Inspired by Portrait mode in Camera, this new video effect puts
              the focus on you, not what’s behind you.
              <sup>
                <a href="#two">2</a>
              </sup>
            </p>
          </div>{" "}
          <div
            style={{ position: "relative" }}
            className="mic-mode  grid-4 white-background"
          >
            <OverlayContent
              name="mic"
              h2="Mic modes"
              name2="mic"
              currentP="Voice Isolation minimizes background noise and puts your voice front and center. When the music or sounds around you are as important as what you have to say, Wide Spectrum leaves the ambient sound unfiltered."
              overlayP="With Voice Isolation, your mic uses machine learning to block ambient and distracting noises (like traffic outside or a TV in the next room) to make sure your voice comes through loud and clear."
              overlayP2="Wide Spectrum lets your friends on FaceTime hear  every sound happening where you are. It’s ideal for taking music lessons or for when you want someone to listen to what’s going on around you."
            >
              <div className="audio-waves">
                <AudioIcon />
              </div>
              <div className="flex-content">
                <MobileLayout className={`vertical `} id="mic"></MobileLayout>
              </div>
            </OverlayContent>
          </div>
          <FaceTimeLinks />
        </div>
      </div>
    </div>
  );
};
