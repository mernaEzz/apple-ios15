import React from "react";
import dayImg from "../../../../../assets/images/day.png";
import inviteImg from "../../../../../assets/images/invite-mobile.jpg";
import { OverlayContent } from "../../../overlayContent/overlayContent";
import "./faceTimeLinks.css";
export const FaceTimeLinks = () => {
  return (
    <>
      <div
        style={{ position: "relative" }}
        className="face-time-links grid-4 white-background"
      >
        <OverlayContent
          h2="FaceTime links"
          name="faceTime-links"
          h3="Schedule and share FaceTime calls with a unique web link."
          imgSrc={dayImg}
          alt="Schedule "
          overlayP="Make a unique FaceTime web link to easily set up a call for later or share the link with a group. Share it with your friends in Messages, Mail, or third‑party apps like WhatsApp. You can also generate a link for an event in Calendar, so everyone will know exactly where and when to meet."
        ></OverlayContent>
      </div>
      <div className="invite-facetime grid-8 white-background">
        <h2>Invite anyone to FaceTime</h2>
        <p name=" links">
          Now you can send friends and family a link to connect on FaceTime —
          even if they’re using Windows or Android.1 And it’s still end-to-end
          encrypted, so your call is as private and secure as any other FaceTime
          call.
        </p>
        <div className="row">
          <img src={inviteImg} alt="invite-mobile" />
        </div>
      </div>
    </>
  );
};
