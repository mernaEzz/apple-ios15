import React, { Component } from "react";
import { MobileLayout } from "../../mobileLayout/mobileLayout";
import { OverlayContent } from "../../overlayContent/overlayContent";
import watchOverlay from "../../../../assets/images/watch-overlay.jpg";
import "./watchListenShare.css";
export class WatchListenShare extends Component {
  render() {
    return (
      <div className="gray-background ">
        <div className="grid">
          <div className="content  watch-listen-share">
            <div style={{ position: "relative" }} className="watch-together">
              <OverlayContent
                name="watch-together"
                h2="Watch together"
                currentP="Stream movies and TV shows while on a FaceTime call with friends. With synced playback and controls, you’ll see everyone laugh, jump, and react to the same moments at the same time. And the volume automatically adjusts, so you can keep talking while you watch."
                overlayP="Watch your favorite videos, movies, and TV shows in sync with friends. The following apps and streaming services will support SharePlay later this year."
                imgSrc={watchOverlay}
              >
                {" "}
                <div className="flex-content">
                  <MobileLayout
                    className={`vertical `}
                    id="watch"
                  ></MobileLayout>
                </div>
              </OverlayContent>
            </div>
            <div className="listen-together white-background">
              <h2>Listen together</h2>
              <p>
                Get together and listen to an album with friends. The whole
                group can see what's next and add songs to a shared queue with
                synced playback and easy-to-use controls.
              </p>
              <div className="flex-content">
                <MobileLayout className="vertical" id="listen"></MobileLayout>
              </div>
            </div>

            <div className="share-screen  white-background">
              <h2>Share your screen</h2>
              <p>
                Share your screen with everyone in your FaceTime call. Browse
                apartment listings, swipe through a photo album, or teach
                someone a new skill, all while seeing and talking to one
                another.
              </p>{" "}
              <div className="flex-content">
                <MobileLayout className="vertical" id="share"></MobileLayout>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
