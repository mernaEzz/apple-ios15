import React, { Component } from "react";
import accessabilityImg from "../../../../../assets/images/accessability.png";
import weatherImg from "../../../../../assets/images/weather.png";
import notesImg from "../../../../../assets/images/notesIcon.png";
import translateImg from "../../../../../assets/images/translate.png";
import widgetImg from "../../../../../assets/images/widget.png";
import findImg from "../../../../../assets/images/find.png";
import appleImg from "../../../../../assets/images/apple.png";
import "./options.css";
export class Options extends Component {
  state = {
    data: [
      {
        img: accessabilityImg,
        title: "Accessibility. ",
        content:
          "Customize display and text size settings for each app. Bold or enlarge text, increase contrast, invert colors, and more for only the apps you want. Explore people, objects, text, and tables within images in more detail with VoiceOver. Navigate receipts and nutrition label values in logical order. And move your finger over a photo to discover a person’s position relative to other objects in the image.",
      },
      {
        img: weatherImg,
        title: "Weather. ",
        content:
          " A fresh new look includes graphical displays of weather data and beautifully redesigned animated backgrounds that make Weather more engaging than ever.",
      },
      {
        img: notesImg,
        title: "Notes. ",
        content:
          "Productivity updates in Notes enable you to organize with tags and collaborate in new ways with mentions and an Activity view.",
      },
      {
        img: translateImg,
        title: "Translate. ",
        content:
          "System-wide translation lets you translate text that you select, even in many third-party apps. In the app, Auto Translate and face to face view improve conversation flow and make it easier to follow along.",
      },
      {
        img: widgetImg,
        title: "Widgets. ",
        content:
          " Enjoy all-new widgets for Find My, Game Center, App Store, Sleep, Mail, and Contacts.",
      },
      {
        img: findImg,
        title: "Find My. ",
        content:
          " See your family’s and friends’ locations with continuous updates. Locate your devices using the Find My network if they’ve been erased, or for up to 24 hours even after they’ve been turned off.",
      },
      {
        img: appleImg,
        title: "Apple ID. ",
        content:
          "Account Recovery Contacts makes resetting your password and maintaining access to your account easier than ever. And a new Digital Legacy program lets you designate people as Legacy Contacts so they can access your account in the event of your death.7",
      },
    ],
  };
  render() {
    let { data } = this.state;
    return (
      <div className="options flex-content ">
        {data.map((contentData, id) => {
          return (
            <div key={id} className="options-content large-6">
              <img src={contentData.img} alt={contentData.title} />
              <p>
                <span>{contentData.title}</span>
                {contentData.content}
              </p>
            </div>
          );
        })}
      </div>
    );
  }
}
