import React from "react";
import muchImg from "../../../../assets/images/much.jpg";
import { Links } from "../../../elements/links/links";
import { Options } from "./options/options";
import "./soMuch.css";
export const SoMuch = () => {
  return (
    <div className="so-much gray-background">
      <div className="so-much-content">
        <h4>And so much more.</h4>
        <img src={muchImg} alt="so-much-img" />
        <Options />
        <div className="full-list">
          <h5>See the full list of what’s new in iOS 15.</h5>
          <Links to="/features">
            Learn more<i className="fa fa-angle-right	"></i>
          </Links>
        </div>
      </div>
    </div>
  );
};
