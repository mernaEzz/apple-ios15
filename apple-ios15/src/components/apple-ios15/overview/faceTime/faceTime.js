import React from "react";
import { HealthSpotLayout } from "../healthSpotLayout/healthSpotLayout";
import shareplayImg from "../../../../assets/images/shareplay.jpg";
import shareplayMobImg from "../../../../assets/images/shareplay-mobile.jpg";
import "./faceTime.css";
export const FaceTime = () => {
  return (
    <HealthSpotLayout
      id="face-time"
      name="face-time"
      h1="FaceTime"
      h2="Introducing SharePlay"
      text=" Keep FaceTime conversations going as you watch TV shows and
               movies, listen to music, or share your screen. SharePlay is an
               entirely new way to have experiences with family and friends, no
               matter the distance."
      src={shareplayImg}
      mobileImgSrc={shareplayMobImg}
      alt="shareplay-img"
    />
  );
};
