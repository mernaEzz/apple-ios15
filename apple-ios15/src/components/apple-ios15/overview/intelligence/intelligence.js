import React from "react";
export const Intelligence = () => {
  return (
    <div className="intelligence gray-background title-div">
      <div className="grid">
        {" "}
        <div className="content">
          <h1>
            Intelligence designed to
            <br /> be helpful.
          </h1>
          <p>
            There is so much rich information in your photos, from memorable
            places you’ve visited to handwritten family recipes. iOS 15 uses
            secure on-device intelligence to help you discover more in your
            photos, quickly find what you’re looking for, and relive special
            moments.
          </p>
        </div>
      </div>
    </div>
  );
};
