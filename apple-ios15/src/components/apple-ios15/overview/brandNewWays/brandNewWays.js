import React from "react";

export const BrandNewWays = () => {
  return (
    <div className="brand-new-ways gray-background title-div">
      <div className="grid">
        {" "}
        <div className="content">
          <h1>Brand-new ways of looking at the world.</h1>
          <p>
            Exploration has never looked better. Maps delivers incredible new
            details that go beyond simply taking you from point A to point B.
            Safari offers even more ways for you to explore online. And Wallet
            provides a single, secure place to store your most personal
            information.
          </p>
        </div>
      </div>
    </div>
  );
};
