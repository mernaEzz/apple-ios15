import React, { Component } from "react";
import { AudioIcon } from "../../elements/audioIcon/audioIcon";
import "./overlayContent.css";
export class OverlayContent extends Component {
  state = {
    changeContent: false,
  };
  handlePlusClicked = () => {
    let { changeContent } = this.state;
    this.setState({ changeContent: !changeContent });
  };
  render() {
    let { changeContent } = this.state;
    return (
      <>
        <div className="current-content " id={changeContent ? "ooo" : ""}>
          {this.props.name === "mic" && (
            <>
              <h2
                style={{
                  color: changeContent ? "#fff" : "#f63e54",
                  position: "relative",
                  zIndex: "99",
                }}
              >
                {this.props.h2}
              </h2>
            </>
          )}
          {this.props.name === "watch-together" && (
            <>
              <h2
                style={{
                  color: changeContent ? "#fff" : "#f63e54",
                  position: "relative",
                  zIndex: "99",
                }}
              >
                {this.props.h2}
              </h2>
            </>
          )}
          {(this.props.name === "faceTime-links" ||
            this.props.name === "apple-music") && (
            <>
              {" "}
              <h2
                style={{
                  color:
                    changeContent && this.props.name === "faceTime-links"
                      ? "#fff"
                      : this.props.name === "apple-music"
                      ? "#fff"
                      : "#f63e54",
                  position: "relative",
                  zIndex: "99",
                }}
              >
                {this.props.h2}
              </h2>{" "}
              <div className="flex">
                <img src={this.props.imgSrc} alt={this.props.alt} />
                <h3>{this.props.h3}</h3>
              </div>
            </>
          )}
          {this.props.name === "privacy" && (
            <>
              {" "}
              <h2
                style={{
                  color: changeContent ? "#fff" : "#027fff",
                  position: "relative",
                  zIndex: "99",
                }}
              >
                {this.props.h2}
              </h2>{" "}
              <div className="flex">
                <img src={this.props.imgSrc} alt={this.props.alt} />
                <h3>{this.props.h3}</h3>
              </div>
            </>
          )}
          {this.props.name === "voice-search" && (
            <>
              {" "}
              <h2
                style={{
                  color: changeContent ? "#fff" : "#049cb7",
                  position: "relative",
                  zIndex: "99",
                }}
              >
                {this.props.h2}
              </h2>{" "}
              <div className="flex">
                <h3>{this.props.h3}</h3>
                <AudioIcon />
              </div>
            </>
          )}
          {this.props.name === "live-text" && (
            <>
              <h2
                style={{
                  color: changeContent ? "#fff" : "#3d44c3",
                  position: "relative",
                  zIndex: "99",
                }}
              >
                {this.props.h2}
              </h2>
              <p>
                {this.props.p}
                <sup>
                  <a href="#five">5</a>
                </sup>
              </p>
              <img
                src={this.props.src}
                alt="live-img"
                className="live-text-img"
              />
              <img
                src={this.props.mobileSrc}
                alt="live-img"
                className="live-text-mobile"
              />
            </>
          )}
          <p style={{ opacity: changeContent ? "0" : "1" }}>
            {this.props.currentP}
            {this.props.name === "mic" && (
              <sup>
                <a href="#two">2</a>
              </sup>
            )}
          </p>
          {changeContent ? null : this.props.children}
        </div>
        <div
          className="overlay-content"
          id={changeContent ? "visible-overlay" : ""}
          style={
            {
              // opacity: changeContent ? "1" : "0",
              // visibility: changeContent ? "visible" : "hidden",
            }
          }
        >
          {/* <h1>{this.props.h1}</h1>{" "} */}
          <div>
            <p
              style={{
                transform: changeContent
                  ? "translateY(0)"
                  : "translateY(-20px)",
              }}
            >
              {this.props.overlayP}
              <br />
            </p>{" "}
            <br />
            <br />
            <p
              className="overlay-p2"
              style={{
                transform: changeContent
                  ? "translateY(0)"
                  : "translateY(-20px)",
              }}
            >
              {this.props.overlayP2}
            </p>
            <img src={this.props.imgSrc} alt="icons" />{" "}
          </div>
        </div>
        <div
          className="icon"
          id={changeContent ? "transformed-icon" : "default-icon"}
          onClick={this.handlePlusClicked}
          style={{ position: "relative" }}
        >
          {" "}
          <span className="circle">
            {" "}
            <svg
              className="tile-icon-alt"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <path d="M18.5,8.51h-7v-7A1.5,1.5,0,0,0,10,0h0A1.5,1.5,0,0,0,8.5,1.5v7h-7a1.5,1.5,0,0,0,0,3h7v7A1.5,1.5,0,0,0,10,20h0a1.5,1.5,0,0,0,1.5-1.5v-7h7a1.5,1.5,0,0,0,0-3Z"></path>
            </svg>
          </span>
        </div>
      </>
    );
  }
}
