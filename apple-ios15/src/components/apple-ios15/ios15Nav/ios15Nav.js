import React from "react";
import { Links } from "../../elements/links/links";
import "./ios15Nav.css";
export class Ios15Nav extends React.PureComponent {
  state = {
    expanded: false,
    border: false,
  };
  componentDidMount() {
    window.addEventListener("scroll", this.handleScrollPosition);
  }
  handleScrollPosition = () => {
    let { border } = this.state;
    if (window.scrollY >= 60) {
      border = true;
    } else {
      border = false;
    }
    this.setState({ border });
  };
  handleExpandMenu = () => {
    let { expanded } = this.state;
    this.setState({ expanded: !expanded });
  };
  render() {
    let { expanded, border } = this.state;
    return (
      <nav className="navs">
        <div
          className={`${expanded ? "expanded-back " : ""} back`}
          onClick={this.handleExpandMenu}
        ></div>

        <div
          className={`${expanded ? "expanded " : ""} ios15-nav`}
          id={border ? "border-bottom" : ""}
        >
          <div className="ios15-nav-content">
            <div className="title">
              <h5>
                <Links to="/">iOS 15</Links>
              </h5>
              <div className="arrow" onClick={this.handleExpandMenu}></div>
            </div>
            <div className="links">
              <Links to="/apple-ios15" className="overview">
                Overview
              </Links>
              <Links to="/features" className="features">
                All New Features
              </Links>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}
