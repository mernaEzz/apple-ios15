import React, { useEffect } from "react";
import { Navbar } from "../../navbar/navbar";
import { Ios15Nav } from "../../apple-ios15/ios15Nav/ios15Nav";
import { Info } from "./info/info";
import { Footer } from "../footer/footer";
import "./features.css";
import { Header } from "./header/header";
import { KeyFeatures } from "./keyFeatures/keyFeatures";
import { Helmet, HelmetProvider } from "react-helmet-async";

export const Features = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  });
  return (
    <>
      <HelmetProvider>
        <Helmet>
          <title>iOS 15 - features - Apple(EG)</title>
        </Helmet>
      </HelmetProvider>

      <div className="features">
        <Navbar />
        <Ios15Nav />
        <div className="section-content">
          <Header />
          <KeyFeatures />
        </div>
        <Info />
        <Footer />
      </div>
    </>
  );
};
