import React, { Component } from "react";
import "./info.css";
export class Info extends Component {
  state = {
    info: [
      { id: "key1", title: "Available on iPhone with A12 Bionic and later." },
      {
        id: "key2",
        title:
          "Available on iPhone with A12 Bionic and later. Live Text is currently supported in English, Chinese, French, Italian, German, Portuguese, and Spanish.      ",
      },
      {
        id: "key3",
        title:
          "Users with non-Apple devices can join using the latest version of Chrome or Edge. Sending video requires H.264 video encoding support. Some shared content may require a subscription to view.",
      },
      {
        id: "key4",
        title:
          "Available on iPhone with A12 Bionic and later. CarPlay support coming in a software update later this year.      ",
      },
      { id: "key5", title: "Available on iPhone 8 and later.      " },
      { id: "key6", title: "Available on all AR-enabled iPhone models.      " },
      {
        id: "key7",
        title: "Available on iPhone XS, iPhone XR, and later.      ",
      },
      { id: "key8", title: "Apple Music requires a subscription.      " },
      { id: "key9", title: "Available on iPhone 7 and later.      " },
    ],
  };
  render() {
    let { info } = this.state;
    return (
      <div className="info gray">
        <div className="info-content">
          <p>* Available this spring.</p>
          <ol>
            {info.map((content, id) => {
              return (
                <li key={id} id={content.id}>
                  {content.title}
                </li>
              );
            })}{" "}
          </ol>
          <p>
            Features are subject to change. Some features, applications, and
            services may not be available in all regions or all languages.
          </p>
        </div>
      </div>
    );
  }
}
