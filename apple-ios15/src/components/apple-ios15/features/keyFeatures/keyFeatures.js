import React from "react";
import { Data } from "./data";
import "./keyFeatures.css";
export const KeyFeatures = () => {
  return (
    <div className="key-features">
      <h2>Key Features and Enhancements</h2>
      {Data.map((contentData, id) => {
        return (
          <div className="row key-feature-content" key={id}>
            <div className="left-title">
              <h3>{contentData.title}</h3>
            </div>
            <div className="right-content">
              {contentData.content.map((data, dataId) => {
                return (
                  <div className="content-data" key={dataId}>
                    <h4>{data.header}</h4>
                    <p>
                      {data.details}
                      <sup>
                        <a href="#key1">{data.sup}</a>
                      </sup>
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        );
      })}
      <h2 className="even">Even More</h2>
      {Data.map((contentData, id) => {
        return (
          <div className="row key-feature-content" key={id}>
            <div className="left-title">
              <h3>{contentData.title}</h3>
            </div>
            <div className="right-content">
              {contentData.content.map((data, dataId) => {
                return (
                  <div className="content-data" key={dataId}>
                    <h4>{data.header}</h4>
                    <p>
                      {data.details}
                      <sup>
                        <a href="#key1">{data.sup}</a>
                      </sup>
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};
