export const Data = [
  {
    title: "FaceTime",
    content: [
      {
        header: "SharePlay: Watch together",
        details:
          "Bring movies and TV shows into your FaceTime calls and enjoy a rich, real-time connection with your friends while watching the same content.",
      },
      {
        header: "SharePlay: Listen together        ",
        details: "Share music with your friends right in your FaceTime calls.",
      },
      {
        header: "SharePlay: Share your screen        ",
        details:
          "Share your screen to bring web pages, apps, and more into your conversation on FaceTime.",
      },
      {
        header: "SharePlay: Synced playback        ",
        details:
          "Pause, rewind, fast-forward, or jump to a different scene — everyone’s playback remains in perfect sync.",
      },
      {
        header: "SharePlay: Shared music queue        ",
        details:
          "When listening together, anyone in the call can add songs to the shared queue.",
      },
      {
        header: "SharePlay: Smart volume        ",
        details:
          "Dynamically responsive volume controls automatically adjust audio so you can hear your friends even during a loud scene or climactic chorus.",
      },
      {
        header: "SharePlay: Multiple device support        ",
        details:
          "Connect over FaceTime on your iPad while watching video on your Apple TV or listening to music on your iPhone.",
      },
      {
        header: "SharePlay: Connect through audio, video, and text",
        details:
          "Access your group’s Messages thread right from the FaceTime controls and choose the mode of communication that matches the moment.",
      },
      {
        header: "Portrait mode        ",
        details:
          "Inspired by the portraits you take in the Camera app, Portrait mode in FaceTime blurs your background and puts the focus on you.",
        sup: 1,
      },
      {
        header: "Grid view        ",
        details:
          "Lets you see people in your Group FaceTime calls in the same-size tiles, and highlights the current speaker so it’s easy to know who’s talking. You’ll see up to six faces in the grid at a time.",
      },
      {
        header: "Spatial audio        ",
        details:
          "Creates a sound field that helps conversations flow as easily as they do face to face. Your friends’ voices are spread out to sound like they’re coming from the direction in which they’re positioned on the call.",
        sup: 1,
      },
      {
        header: "Voice Isolation mode        ",
        details:
          "This microphone mode spotlights your voice by using machine learning to identify ambient noises and block them out. So a leaf blower outside or a dog barking in the next room won’t interrupt your call.",
        sup: 1,
      },
      {
        header: "Wide Spectrum mode        ",
        details:
          "This microphone mode brings every single sound into your call. It’s ideal for when you’re taking music lessons or want your friend to hear everything that’s happening in the space you’re in.",
        sup: 1,
      },
      {
        header: "FaceTime links        ",
        details:
          "Invite your friends into a FaceTime call using a web link you can share anywhere.",
      },
      {
        header: "Join FaceTime on the web        ",
        details:
          "Invite anyone to join you in a FaceTime call, even friends who don’t have an Apple device.3 They can join you for one-on-one and Group FaceTime calls right from their browser instantly — no login necessary.",
      },
      {
        header: "Calendar integration        ",
        details:
          "Generate a web link for a FaceTime call while creating an event in Calendar, so everyone knows exactly where to meet and when.",
      },
      {
        header: "Mute alerts        ",
        details:
          "Lets you know when you’re talking while muted. Tap the alert to quickly unmute and make sure your voice is heard.",
      },
      {
        header: "Zoom        ",
        details:
          "An optical zoom control for your back camera helps you zero in on what matters when you’re on a FaceTime call.",
      },
    ],
  },
  {
    title: "Messages    ",
    content: [
      {
        header: "Shared with You        ",
        details:
          "Content sent to you over Messages automatically appears in a new Shared with You section in the corresponding app, so you can enjoy it when it’s convenient for you. Shared with You will be featured in Photos, Safari, Apple Music, Apple Podcasts, and the Apple TV app.",
      },
      {
        header: "Shared with You: Pins        ",
        details:
          "For content that’s especially interesting to you, you can quickly pin it in Messages, and it will be elevated in Shared with You, Messages search, and the Details view of the conversation.",
      },
      {
        header: "Shared with You: Continue the conversation",
        details:
          "Alongside shared content in the corresponding apps, you can see who sent it and tap the sender to view the associated messages and continue the conversation — right from the app — without going to Messages.",
      },
      {
        header: "Shared with You: Photos        ",
        details:
          "Photos sent to you over Messages automatically appear in your Photos app. Your library includes the photos you care about most — like the ones you were there for. And in For You, the broader set of shared photos will be featured in a new Shared with You section, your Memories, and your Featured Photos.",
      },
      {
        header: "Shared with You: Safari        ",
        details:
          "Interesting articles, recipes, and other links sent over Messages automatically appear in the new Shared with You section on the Safari start page and in the sidebar.",
      },
      {
        header: "Shared with You: Apple Music        ",
        details:
          "Music sent over Messages automatically appears in the new Shared with You section of Listen Now in Apple Music.",
      },
      {
        header: "Shared with You: Apple Podcasts        ",
        details:
          "Podcast shows and episodes sent over Messages automatically appear in the new Shared with You section of Listen Now in Apple Podcasts.",
      },
      {
        header: "Shared with You: Apple TV app        ",
        details:
          "Movies and shows sent over Messages automatically appear in the new Shared with You section of Watch Now in the Apple TV app.",
      },
      {
        header: "Photo collections        ",
        details:
          "Enjoy multiple photos as beautiful collections in your Messages conversations. A handful of images appears as a glanceable collage and a larger set as an elegant stack that you can swipe through. Tap to view them as a grid and easily add a Tapback or inline reply.",
      },
      {
        header: "Easily save photos        ",
        details:
          "You can quickly save photos sent to you by tapping a new save button right in the Messages conversation.",
      },
      {
        header: "SMS filtering for Brazil        ",
        details:
          "Messages features on-device intelligence that filters unwanted SMS messages, organizing them into Promotional, Transactional, and Junk folders so your inbox can stay clutter‑free.",
      },
      {
        header: "Notification options in Messages for India and China",
        details:
          "Turn notifications on or off for unknown senders, transactions, and promotions to determine which types of messages you want to receive notifications for.",
      },
      {
        header: "Switch phone numbers in Messages        ",
        details:
          "Switch between phone numbers in the middle of a conversation on an iPhone with Dual SIM.",
      },
    ],
  },
  {
    title: "FaceTime",
    content: [
      {
        header: "SharePlay: Watch together",
        details:
          "Bring movies and TV shows into your FaceTime calls and enjoy a rich, real-time connection with your friends while watching the same content.",
      },
      {
        header: "SharePlay: Listen together        ",
        details: "Share music with your friends right in your FaceTime calls.",
      },
      {
        header: "SharePlay: Share your screen        ",
        details:
          "Share your screen to bring web pages, apps, and more into your conversation on FaceTime.",
      },
      {
        header: "SharePlay: Synced playback        ",
        details:
          "Pause, rewind, fast-forward, or jump to a different scene — everyone’s playback remains in perfect sync.",
      },
      {
        header: "SharePlay: Shared music queue        ",
        details:
          "When listening together, anyone in the call can add songs to the shared queue.",
      },
      {
        header: "SharePlay: Smart volume        ",
        details:
          "Dynamically responsive volume controls automatically adjust audio so you can hear your friends even during a loud scene or climactic chorus.",
      },
      {
        header: "SharePlay: Multiple device support        ",
        details:
          "Connect over FaceTime on your iPad while watching video on your Apple TV or listening to music on your iPhone.",
      },
      {
        header: "SharePlay: Connect through audio, video, and text",
        details:
          "Access your group’s Messages thread right from the FaceTime controls and choose the mode of communication that matches the moment.",
      },
      {
        header: "Portrait mode        ",
        details:
          "Inspired by the portraits you take in the Camera app, Portrait mode in FaceTime blurs your background and puts the focus on you.",
        sup: 1,
      },
      {
        header: "Grid view        ",
        details:
          "Lets you see people in your Group FaceTime calls in the same-size tiles, and highlights the current speaker so it’s easy to know who’s talking. You’ll see up to six faces in the grid at a time.",
      },
      {
        header: "Spatial audio        ",
        details:
          "Creates a sound field that helps conversations flow as easily as they do face to face. Your friends’ voices are spread out to sound like they’re coming from the direction in which they’re positioned on the call.",
        sup: 1,
      },
      {
        header: "Voice Isolation mode        ",
        details:
          "This microphone mode spotlights your voice by using machine learning to identify ambient noises and block them out. So a leaf blower outside or a dog barking in the next room won’t interrupt your call.",
        sup: 1,
      },
      {
        header: "Wide Spectrum mode        ",
        details:
          "This microphone mode brings every single sound into your call. It’s ideal for when you’re taking music lessons or want your friend to hear everything that’s happening in the space you’re in.",
        sup: 1,
      },
      {
        header: "FaceTime links        ",
        details:
          "Invite your friends into a FaceTime call using a web link you can share anywhere.",
      },
      {
        header: "Join FaceTime on the web        ",
        details:
          "Invite anyone to join you in a FaceTime call, even friends who don’t have an Apple device.3 They can join you for one-on-one and Group FaceTime calls right from their browser instantly — no login necessary.",
      },
      {
        header: "Calendar integration        ",
        details:
          "Generate a web link for a FaceTime call while creating an event in Calendar, so everyone knows exactly where to meet and when.",
      },
      {
        header: "Mute alerts        ",
        details:
          "Lets you know when you’re talking while muted. Tap the alert to quickly unmute and make sure your voice is heard.",
      },
      {
        header: "Zoom        ",
        details:
          "An optical zoom control for your back camera helps you zero in on what matters when you’re on a FaceTime call.",
      },
    ],
  },
  {
    title: "Messages    ",
    content: [
      {
        header: "Shared with You        ",
        details:
          "Content sent to you over Messages automatically appears in a new Shared with You section in the corresponding app, so you can enjoy it when it’s convenient for you. Shared with You will be featured in Photos, Safari, Apple Music, Apple Podcasts, and the Apple TV app.",
      },
      {
        header: "Shared with You: Pins        ",
        details:
          "For content that’s especially interesting to you, you can quickly pin it in Messages, and it will be elevated in Shared with You, Messages search, and the Details view of the conversation.",
      },
      {
        header: "Shared with You: Continue the conversation",
        details:
          "Alongside shared content in the corresponding apps, you can see who sent it and tap the sender to view the associated messages and continue the conversation — right from the app — without going to Messages.",
      },
      {
        header: "Shared with You: Photos        ",
        details:
          "Photos sent to you over Messages automatically appear in your Photos app. Your library includes the photos you care about most — like the ones you were there for. And in For You, the broader set of shared photos will be featured in a new Shared with You section, your Memories, and your Featured Photos.",
      },
      {
        header: "Shared with You: Safari        ",
        details:
          "Interesting articles, recipes, and other links sent over Messages automatically appear in the new Shared with You section on the Safari start page and in the sidebar.",
      },
      {
        header: "Shared with You: Apple Music        ",
        details:
          "Music sent over Messages automatically appears in the new Shared with You section of Listen Now in Apple Music.",
      },
      {
        header: "Shared with You: Apple Podcasts        ",
        details:
          "Podcast shows and episodes sent over Messages automatically appear in the new Shared with You section of Listen Now in Apple Podcasts.",
      },
      {
        header: "Shared with You: Apple TV app        ",
        details:
          "Movies and shows sent over Messages automatically appear in the new Shared with You section of Watch Now in the Apple TV app.",
      },
      {
        header: "Photo collections        ",
        details:
          "Enjoy multiple photos as beautiful collections in your Messages conversations. A handful of images appears as a glanceable collage and a larger set as an elegant stack that you can swipe through. Tap to view them as a grid and easily add a Tapback or inline reply.",
      },
      {
        header: "Easily save photos        ",
        details:
          "You can quickly save photos sent to you by tapping a new save button right in the Messages conversation.",
      },
      {
        header: "SMS filtering for Brazil        ",
        details:
          "Messages features on-device intelligence that filters unwanted SMS messages, organizing them into Promotional, Transactional, and Junk folders so your inbox can stay clutter‑free.",
      },
      {
        header: "Notification options in Messages for India and China",
        details:
          "Turn notifications on or off for unknown senders, transactions, and promotions to determine which types of messages you want to receive notifications for.",
      },
      {
        header: "Switch phone numbers in Messages        ",
        details:
          "Switch between phone numbers in the middle of a conversation on an iPhone with Dual SIM.",
      },
    ],
  },
  {
    title: "FaceTime",
    content: [
      {
        header: "SharePlay: Watch together",
        details:
          "Bring movies and TV shows into your FaceTime calls and enjoy a rich, real-time connection with your friends while watching the same content.",
      },
      {
        header: "SharePlay: Listen together        ",
        details: "Share music with your friends right in your FaceTime calls.",
      },
      {
        header: "SharePlay: Share your screen        ",
        details:
          "Share your screen to bring web pages, apps, and more into your conversation on FaceTime.",
      },
      {
        header: "SharePlay: Synced playback        ",
        details:
          "Pause, rewind, fast-forward, or jump to a different scene — everyone’s playback remains in perfect sync.",
      },
      {
        header: "SharePlay: Shared music queue        ",
        details:
          "When listening together, anyone in the call can add songs to the shared queue.",
      },
      {
        header: "SharePlay: Smart volume        ",
        details:
          "Dynamically responsive volume controls automatically adjust audio so you can hear your friends even during a loud scene or climactic chorus.",
      },
      {
        header: "SharePlay: Multiple device support        ",
        details:
          "Connect over FaceTime on your iPad while watching video on your Apple TV or listening to music on your iPhone.",
      },
      {
        header: "SharePlay: Connect through audio, video, and text",
        details:
          "Access your group’s Messages thread right from the FaceTime controls and choose the mode of communication that matches the moment.",
      },
      {
        header: "Portrait mode        ",
        details:
          "Inspired by the portraits you take in the Camera app, Portrait mode in FaceTime blurs your background and puts the focus on you.",
        sup: 1,
      },
      {
        header: "Grid view        ",
        details:
          "Lets you see people in your Group FaceTime calls in the same-size tiles, and highlights the current speaker so it’s easy to know who’s talking. You’ll see up to six faces in the grid at a time.",
      },
      {
        header: "Spatial audio        ",
        details:
          "Creates a sound field that helps conversations flow as easily as they do face to face. Your friends’ voices are spread out to sound like they’re coming from the direction in which they’re positioned on the call.",
        sup: 1,
      },
      {
        header: "Voice Isolation mode        ",
        details:
          "This microphone mode spotlights your voice by using machine learning to identify ambient noises and block them out. So a leaf blower outside or a dog barking in the next room won’t interrupt your call.",
        sup: 1,
      },
      {
        header: "Wide Spectrum mode        ",
        details:
          "This microphone mode brings every single sound into your call. It’s ideal for when you’re taking music lessons or want your friend to hear everything that’s happening in the space you’re in.",
        sup: 1,
      },
      {
        header: "FaceTime links        ",
        details:
          "Invite your friends into a FaceTime call using a web link you can share anywhere.",
      },
      {
        header: "Join FaceTime on the web        ",
        details:
          "Invite anyone to join you in a FaceTime call, even friends who don’t have an Apple device.3 They can join you for one-on-one and Group FaceTime calls right from their browser instantly — no login necessary.",
      },
      {
        header: "Calendar integration        ",
        details:
          "Generate a web link for a FaceTime call while creating an event in Calendar, so everyone knows exactly where to meet and when.",
      },
      {
        header: "Mute alerts        ",
        details:
          "Lets you know when you’re talking while muted. Tap the alert to quickly unmute and make sure your voice is heard.",
      },
      {
        header: "Zoom        ",
        details:
          "An optical zoom control for your back camera helps you zero in on what matters when you’re on a FaceTime call.",
      },
    ],
  },
  {
    title: "Messages    ",
    content: [
      {
        header: "Shared with You        ",
        details:
          "Content sent to you over Messages automatically appears in a new Shared with You section in the corresponding app, so you can enjoy it when it’s convenient for you. Shared with You will be featured in Photos, Safari, Apple Music, Apple Podcasts, and the Apple TV app.",
      },
      {
        header: "Shared with You: Pins        ",
        details:
          "For content that’s especially interesting to you, you can quickly pin it in Messages, and it will be elevated in Shared with You, Messages search, and the Details view of the conversation.",
      },
      {
        header: "Shared with You: Continue the conversation",
        details:
          "Alongside shared content in the corresponding apps, you can see who sent it and tap the sender to view the associated messages and continue the conversation — right from the app — without going to Messages.",
      },
      {
        header: "Shared with You: Photos        ",
        details:
          "Photos sent to you over Messages automatically appear in your Photos app. Your library includes the photos you care about most — like the ones you were there for. And in For You, the broader set of shared photos will be featured in a new Shared with You section, your Memories, and your Featured Photos.",
      },
      {
        header: "Shared with You: Safari        ",
        details:
          "Interesting articles, recipes, and other links sent over Messages automatically appear in the new Shared with You section on the Safari start page and in the sidebar.",
      },
      {
        header: "Shared with You: Apple Music        ",
        details:
          "Music sent over Messages automatically appears in the new Shared with You section of Listen Now in Apple Music.",
      },
      {
        header: "Shared with You: Apple Podcasts        ",
        details:
          "Podcast shows and episodes sent over Messages automatically appear in the new Shared with You section of Listen Now in Apple Podcasts.",
      },
      {
        header: "Shared with You: Apple TV app        ",
        details:
          "Movies and shows sent over Messages automatically appear in the new Shared with You section of Watch Now in the Apple TV app.",
      },
      {
        header: "Photo collections        ",
        details:
          "Enjoy multiple photos as beautiful collections in your Messages conversations. A handful of images appears as a glanceable collage and a larger set as an elegant stack that you can swipe through. Tap to view them as a grid and easily add a Tapback or inline reply.",
      },
      {
        header: "Easily save photos        ",
        details:
          "You can quickly save photos sent to you by tapping a new save button right in the Messages conversation.",
      },
      {
        header: "SMS filtering for Brazil        ",
        details:
          "Messages features on-device intelligence that filters unwanted SMS messages, organizing them into Promotional, Transactional, and Junk folders so your inbox can stay clutter‑free.",
      },
      {
        header: "Notification options in Messages for India and China",
        details:
          "Turn notifications on or off for unknown senders, transactions, and promotions to determine which types of messages you want to receive notifications for.",
      },
      {
        header: "Switch phone numbers in Messages        ",
        details:
          "Switch between phone numbers in the middle of a conversation on an iPhone with Dual SIM.",
      },
    ],
  },
  {
    title: "FaceTime",
    content: [
      {
        header: "SharePlay: Watch together",
        details:
          "Bring movies and TV shows into your FaceTime calls and enjoy a rich, real-time connection with your friends while watching the same content.",
      },
      {
        header: "SharePlay: Listen together        ",
        details: "Share music with your friends right in your FaceTime calls.",
      },
      {
        header: "SharePlay: Share your screen        ",
        details:
          "Share your screen to bring web pages, apps, and more into your conversation on FaceTime.",
      },
      {
        header: "SharePlay: Synced playback        ",
        details:
          "Pause, rewind, fast-forward, or jump to a different scene — everyone’s playback remains in perfect sync.",
      },
      {
        header: "SharePlay: Shared music queue        ",
        details:
          "When listening together, anyone in the call can add songs to the shared queue.",
      },
      {
        header: "SharePlay: Smart volume        ",
        details:
          "Dynamically responsive volume controls automatically adjust audio so you can hear your friends even during a loud scene or climactic chorus.",
      },
      {
        header: "SharePlay: Multiple device support        ",
        details:
          "Connect over FaceTime on your iPad while watching video on your Apple TV or listening to music on your iPhone.",
      },
      {
        header: "SharePlay: Connect through audio, video, and text",
        details:
          "Access your group’s Messages thread right from the FaceTime controls and choose the mode of communication that matches the moment.",
      },
      {
        header: "Portrait mode        ",
        details:
          "Inspired by the portraits you take in the Camera app, Portrait mode in FaceTime blurs your background and puts the focus on you.",
        sup: 1,
      },
      {
        header: "Grid view        ",
        details:
          "Lets you see people in your Group FaceTime calls in the same-size tiles, and highlights the current speaker so it’s easy to know who’s talking. You’ll see up to six faces in the grid at a time.",
      },
      {
        header: "Spatial audio        ",
        details:
          "Creates a sound field that helps conversations flow as easily as they do face to face. Your friends’ voices are spread out to sound like they’re coming from the direction in which they’re positioned on the call.",
        sup: 1,
      },
      {
        header: "Voice Isolation mode        ",
        details:
          "This microphone mode spotlights your voice by using machine learning to identify ambient noises and block them out. So a leaf blower outside or a dog barking in the next room won’t interrupt your call.",
        sup: 1,
      },
      {
        header: "Wide Spectrum mode        ",
        details:
          "This microphone mode brings every single sound into your call. It’s ideal for when you’re taking music lessons or want your friend to hear everything that’s happening in the space you’re in.",
        sup: 1,
      },
      {
        header: "FaceTime links        ",
        details:
          "Invite your friends into a FaceTime call using a web link you can share anywhere.",
      },
      {
        header: "Join FaceTime on the web        ",
        details:
          "Invite anyone to join you in a FaceTime call, even friends who don’t have an Apple device.3 They can join you for one-on-one and Group FaceTime calls right from their browser instantly — no login necessary.",
      },
      {
        header: "Calendar integration        ",
        details:
          "Generate a web link for a FaceTime call while creating an event in Calendar, so everyone knows exactly where to meet and when.",
      },
      {
        header: "Mute alerts        ",
        details:
          "Lets you know when you’re talking while muted. Tap the alert to quickly unmute and make sure your voice is heard.",
      },
      {
        header: "Zoom        ",
        details:
          "An optical zoom control for your back camera helps you zero in on what matters when you’re on a FaceTime call.",
      },
    ],
  },
  {
    title: "Messages    ",
    content: [
      {
        header: "Shared with You        ",
        details:
          "Content sent to you over Messages automatically appears in a new Shared with You section in the corresponding app, so you can enjoy it when it’s convenient for you. Shared with You will be featured in Photos, Safari, Apple Music, Apple Podcasts, and the Apple TV app.",
      },
      {
        header: "Shared with You: Pins        ",
        details:
          "For content that’s especially interesting to you, you can quickly pin it in Messages, and it will be elevated in Shared with You, Messages search, and the Details view of the conversation.",
      },
      {
        header: "Shared with You: Continue the conversation",
        details:
          "Alongside shared content in the corresponding apps, you can see who sent it and tap the sender to view the associated messages and continue the conversation — right from the app — without going to Messages.",
      },
      {
        header: "Shared with You: Photos        ",
        details:
          "Photos sent to you over Messages automatically appear in your Photos app. Your library includes the photos you care about most — like the ones you were there for. And in For You, the broader set of shared photos will be featured in a new Shared with You section, your Memories, and your Featured Photos.",
      },
      {
        header: "Shared with You: Safari        ",
        details:
          "Interesting articles, recipes, and other links sent over Messages automatically appear in the new Shared with You section on the Safari start page and in the sidebar.",
      },
      {
        header: "Shared with You: Apple Music        ",
        details:
          "Music sent over Messages automatically appears in the new Shared with You section of Listen Now in Apple Music.",
      },
      {
        header: "Shared with You: Apple Podcasts        ",
        details:
          "Podcast shows and episodes sent over Messages automatically appear in the new Shared with You section of Listen Now in Apple Podcasts.",
      },
      {
        header: "Shared with You: Apple TV app        ",
        details:
          "Movies and shows sent over Messages automatically appear in the new Shared with You section of Watch Now in the Apple TV app.",
      },
      {
        header: "Photo collections        ",
        details:
          "Enjoy multiple photos as beautiful collections in your Messages conversations. A handful of images appears as a glanceable collage and a larger set as an elegant stack that you can swipe through. Tap to view them as a grid and easily add a Tapback or inline reply.",
      },
      {
        header: "Easily save photos        ",
        details:
          "You can quickly save photos sent to you by tapping a new save button right in the Messages conversation.",
      },
      {
        header: "SMS filtering for Brazil        ",
        details:
          "Messages features on-device intelligence that filters unwanted SMS messages, organizing them into Promotional, Transactional, and Junk folders so your inbox can stay clutter‑free.",
      },
      {
        header: "Notification options in Messages for India and China",
        details:
          "Turn notifications on or off for unknown senders, transactions, and promotions to determine which types of messages you want to receive notifications for.",
      },
      {
        header: "Switch phone numbers in Messages        ",
        details:
          "Switch between phone numbers in the middle of a conversation on an iPhone with Dual SIM.",
      },
    ],
  },
  {
    title: "FaceTime",
    content: [
      {
        header: "SharePlay: Watch together",
        details:
          "Bring movies and TV shows into your FaceTime calls and enjoy a rich, real-time connection with your friends while watching the same content.",
      },
      {
        header: "SharePlay: Listen together        ",
        details: "Share music with your friends right in your FaceTime calls.",
      },
      {
        header: "SharePlay: Share your screen        ",
        details:
          "Share your screen to bring web pages, apps, and more into your conversation on FaceTime.",
      },
      {
        header: "SharePlay: Synced playback        ",
        details:
          "Pause, rewind, fast-forward, or jump to a different scene — everyone’s playback remains in perfect sync.",
      },
      {
        header: "SharePlay: Shared music queue        ",
        details:
          "When listening together, anyone in the call can add songs to the shared queue.",
      },
      {
        header: "SharePlay: Smart volume        ",
        details:
          "Dynamically responsive volume controls automatically adjust audio so you can hear your friends even during a loud scene or climactic chorus.",
      },
      {
        header: "SharePlay: Multiple device support        ",
        details:
          "Connect over FaceTime on your iPad while watching video on your Apple TV or listening to music on your iPhone.",
      },
      {
        header: "SharePlay: Connect through audio, video, and text",
        details:
          "Access your group’s Messages thread right from the FaceTime controls and choose the mode of communication that matches the moment.",
      },
      {
        header: "Portrait mode        ",
        details:
          "Inspired by the portraits you take in the Camera app, Portrait mode in FaceTime blurs your background and puts the focus on you.",
        sup: 1,
      },
      {
        header: "Grid view        ",
        details:
          "Lets you see people in your Group FaceTime calls in the same-size tiles, and highlights the current speaker so it’s easy to know who’s talking. You’ll see up to six faces in the grid at a time.",
      },
      {
        header: "Spatial audio        ",
        details:
          "Creates a sound field that helps conversations flow as easily as they do face to face. Your friends’ voices are spread out to sound like they’re coming from the direction in which they’re positioned on the call.",
        sup: 1,
      },
      {
        header: "Voice Isolation mode        ",
        details:
          "This microphone mode spotlights your voice by using machine learning to identify ambient noises and block them out. So a leaf blower outside or a dog barking in the next room won’t interrupt your call.",
        sup: 1,
      },
      {
        header: "Wide Spectrum mode        ",
        details:
          "This microphone mode brings every single sound into your call. It’s ideal for when you’re taking music lessons or want your friend to hear everything that’s happening in the space you’re in.",
        sup: 1,
      },
      {
        header: "FaceTime links        ",
        details:
          "Invite your friends into a FaceTime call using a web link you can share anywhere.",
      },
      {
        header: "Join FaceTime on the web        ",
        details:
          "Invite anyone to join you in a FaceTime call, even friends who don’t have an Apple device.3 They can join you for one-on-one and Group FaceTime calls right from their browser instantly — no login necessary.",
      },
      {
        header: "Calendar integration        ",
        details:
          "Generate a web link for a FaceTime call while creating an event in Calendar, so everyone knows exactly where to meet and when.",
      },
      {
        header: "Mute alerts        ",
        details:
          "Lets you know when you’re talking while muted. Tap the alert to quickly unmute and make sure your voice is heard.",
      },
      {
        header: "Zoom        ",
        details:
          "An optical zoom control for your back camera helps you zero in on what matters when you’re on a FaceTime call.",
      },
    ],
  },
  {
    title: "Messages    ",
    content: [
      {
        header: "Shared with You        ",
        details:
          "Content sent to you over Messages automatically appears in a new Shared with You section in the corresponding app, so you can enjoy it when it’s convenient for you. Shared with You will be featured in Photos, Safari, Apple Music, Apple Podcasts, and the Apple TV app.",
      },
      {
        header: "Shared with You: Pins        ",
        details:
          "For content that’s especially interesting to you, you can quickly pin it in Messages, and it will be elevated in Shared with You, Messages search, and the Details view of the conversation.",
      },
      {
        header: "Shared with You: Continue the conversation",
        details:
          "Alongside shared content in the corresponding apps, you can see who sent it and tap the sender to view the associated messages and continue the conversation — right from the app — without going to Messages.",
      },
      {
        header: "Shared with You: Photos        ",
        details:
          "Photos sent to you over Messages automatically appear in your Photos app. Your library includes the photos you care about most — like the ones you were there for. And in For You, the broader set of shared photos will be featured in a new Shared with You section, your Memories, and your Featured Photos.",
      },
      {
        header: "Shared with You: Safari        ",
        details:
          "Interesting articles, recipes, and other links sent over Messages automatically appear in the new Shared with You section on the Safari start page and in the sidebar.",
      },
      {
        header: "Shared with You: Apple Music        ",
        details:
          "Music sent over Messages automatically appears in the new Shared with You section of Listen Now in Apple Music.",
      },
      {
        header: "Shared with You: Apple Podcasts        ",
        details:
          "Podcast shows and episodes sent over Messages automatically appear in the new Shared with You section of Listen Now in Apple Podcasts.",
      },
      {
        header: "Shared with You: Apple TV app        ",
        details:
          "Movies and shows sent over Messages automatically appear in the new Shared with You section of Watch Now in the Apple TV app.",
      },
      {
        header: "Photo collections        ",
        details:
          "Enjoy multiple photos as beautiful collections in your Messages conversations. A handful of images appears as a glanceable collage and a larger set as an elegant stack that you can swipe through. Tap to view them as a grid and easily add a Tapback or inline reply.",
      },
      {
        header: "Easily save photos        ",
        details:
          "You can quickly save photos sent to you by tapping a new save button right in the Messages conversation.",
      },
      {
        header: "SMS filtering for Brazil        ",
        details:
          "Messages features on-device intelligence that filters unwanted SMS messages, organizing them into Promotional, Transactional, and Junk folders so your inbox can stay clutter‑free.",
      },
      {
        header: "Notification options in Messages for India and China",
        details:
          "Turn notifications on or off for unknown senders, transactions, and promotions to determine which types of messages you want to receive notifications for.",
      },
      {
        header: "Switch phone numbers in Messages        ",
        details:
          "Switch between phone numbers in the middle of a conversation on an iPhone with Dual SIM.",
      },
    ],
  },
  {
    title: "FaceTime",
    content: [
      {
        header: "SharePlay: Watch together",
        details:
          "Bring movies and TV shows into your FaceTime calls and enjoy a rich, real-time connection with your friends while watching the same content.",
      },
      {
        header: "SharePlay: Listen together        ",
        details: "Share music with your friends right in your FaceTime calls.",
      },
      {
        header: "SharePlay: Share your screen        ",
        details:
          "Share your screen to bring web pages, apps, and more into your conversation on FaceTime.",
      },
      {
        header: "SharePlay: Synced playback        ",
        details:
          "Pause, rewind, fast-forward, or jump to a different scene — everyone’s playback remains in perfect sync.",
      },
      {
        header: "SharePlay: Shared music queue        ",
        details:
          "When listening together, anyone in the call can add songs to the shared queue.",
      },
      {
        header: "SharePlay: Smart volume        ",
        details:
          "Dynamically responsive volume controls automatically adjust audio so you can hear your friends even during a loud scene or climactic chorus.",
      },
      {
        header: "SharePlay: Multiple device support        ",
        details:
          "Connect over FaceTime on your iPad while watching video on your Apple TV or listening to music on your iPhone.",
      },
      {
        header: "SharePlay: Connect through audio, video, and text",
        details:
          "Access your group’s Messages thread right from the FaceTime controls and choose the mode of communication that matches the moment.",
      },
      {
        header: "Portrait mode        ",
        details:
          "Inspired by the portraits you take in the Camera app, Portrait mode in FaceTime blurs your background and puts the focus on you.",
        sup: 1,
      },
      {
        header: "Grid view        ",
        details:
          "Lets you see people in your Group FaceTime calls in the same-size tiles, and highlights the current speaker so it’s easy to know who’s talking. You’ll see up to six faces in the grid at a time.",
      },
      {
        header: "Spatial audio        ",
        details:
          "Creates a sound field that helps conversations flow as easily as they do face to face. Your friends’ voices are spread out to sound like they’re coming from the direction in which they’re positioned on the call.",
        sup: 1,
      },
      {
        header: "Voice Isolation mode        ",
        details:
          "This microphone mode spotlights your voice by using machine learning to identify ambient noises and block them out. So a leaf blower outside or a dog barking in the next room won’t interrupt your call.",
        sup: 1,
      },
      {
        header: "Wide Spectrum mode        ",
        details:
          "This microphone mode brings every single sound into your call. It’s ideal for when you’re taking music lessons or want your friend to hear everything that’s happening in the space you’re in.",
        sup: 1,
      },
      {
        header: "FaceTime links        ",
        details:
          "Invite your friends into a FaceTime call using a web link you can share anywhere.",
      },
      {
        header: "Join FaceTime on the web        ",
        details:
          "Invite anyone to join you in a FaceTime call, even friends who don’t have an Apple device.3 They can join you for one-on-one and Group FaceTime calls right from their browser instantly — no login necessary.",
      },
      {
        header: "Calendar integration        ",
        details:
          "Generate a web link for a FaceTime call while creating an event in Calendar, so everyone knows exactly where to meet and when.",
      },
      {
        header: "Mute alerts        ",
        details:
          "Lets you know when you’re talking while muted. Tap the alert to quickly unmute and make sure your voice is heard.",
      },
      {
        header: "Zoom        ",
        details:
          "An optical zoom control for your back camera helps you zero in on what matters when you’re on a FaceTime call.",
      },
    ],
  },
  {
    title: "Messages    ",
    content: [
      {
        header: "Shared with You        ",
        details:
          "Content sent to you over Messages automatically appears in a new Shared with You section in the corresponding app, so you can enjoy it when it’s convenient for you. Shared with You will be featured in Photos, Safari, Apple Music, Apple Podcasts, and the Apple TV app.",
      },
      {
        header: "Shared with You: Pins        ",
        details:
          "For content that’s especially interesting to you, you can quickly pin it in Messages, and it will be elevated in Shared with You, Messages search, and the Details view of the conversation.",
      },
      {
        header: "Shared with You: Continue the conversation",
        details:
          "Alongside shared content in the corresponding apps, you can see who sent it and tap the sender to view the associated messages and continue the conversation — right from the app — without going to Messages.",
      },
      {
        header: "Shared with You: Photos        ",
        details:
          "Photos sent to you over Messages automatically appear in your Photos app. Your library includes the photos you care about most — like the ones you were there for. And in For You, the broader set of shared photos will be featured in a new Shared with You section, your Memories, and your Featured Photos.",
      },
      {
        header: "Shared with You: Safari        ",
        details:
          "Interesting articles, recipes, and other links sent over Messages automatically appear in the new Shared with You section on the Safari start page and in the sidebar.",
      },
      {
        header: "Shared with You: Apple Music        ",
        details:
          "Music sent over Messages automatically appears in the new Shared with You section of Listen Now in Apple Music.",
      },
      {
        header: "Shared with You: Apple Podcasts        ",
        details:
          "Podcast shows and episodes sent over Messages automatically appear in the new Shared with You section of Listen Now in Apple Podcasts.",
      },
      {
        header: "Shared with You: Apple TV app        ",
        details:
          "Movies and shows sent over Messages automatically appear in the new Shared with You section of Watch Now in the Apple TV app.",
      },
      {
        header: "Photo collections        ",
        details:
          "Enjoy multiple photos as beautiful collections in your Messages conversations. A handful of images appears as a glanceable collage and a larger set as an elegant stack that you can swipe through. Tap to view them as a grid and easily add a Tapback or inline reply.",
      },
      {
        header: "Easily save photos        ",
        details:
          "You can quickly save photos sent to you by tapping a new save button right in the Messages conversation.",
      },
      {
        header: "SMS filtering for Brazil        ",
        details:
          "Messages features on-device intelligence that filters unwanted SMS messages, organizing them into Promotional, Transactional, and Junk folders so your inbox can stay clutter‑free.",
      },
      {
        header: "Notification options in Messages for India and China",
        details:
          "Turn notifications on or off for unknown senders, transactions, and promotions to determine which types of messages you want to receive notifications for.",
      },
      {
        header: "Switch phone numbers in Messages        ",
        details:
          "Switch between phone numbers in the middle of a conversation on an iPhone with Dual SIM.",
      },
    ],
  },
  {
    title: "FaceTime",
    content: [
      {
        header: "SharePlay: Watch together",
        details:
          "Bring movies and TV shows into your FaceTime calls and enjoy a rich, real-time connection with your friends while watching the same content.",
      },
      {
        header: "SharePlay: Listen together        ",
        details: "Share music with your friends right in your FaceTime calls.",
      },
      {
        header: "SharePlay: Share your screen        ",
        details:
          "Share your screen to bring web pages, apps, and more into your conversation on FaceTime.",
      },
      {
        header: "SharePlay: Synced playback        ",
        details:
          "Pause, rewind, fast-forward, or jump to a different scene — everyone’s playback remains in perfect sync.",
      },
      {
        header: "SharePlay: Shared music queue        ",
        details:
          "When listening together, anyone in the call can add songs to the shared queue.",
      },
      {
        header: "SharePlay: Smart volume        ",
        details:
          "Dynamically responsive volume controls automatically adjust audio so you can hear your friends even during a loud scene or climactic chorus.",
      },
      {
        header: "SharePlay: Multiple device support        ",
        details:
          "Connect over FaceTime on your iPad while watching video on your Apple TV or listening to music on your iPhone.",
      },
      {
        header: "SharePlay: Connect through audio, video, and text",
        details:
          "Access your group’s Messages thread right from the FaceTime controls and choose the mode of communication that matches the moment.",
      },
      {
        header: "Portrait mode        ",
        details:
          "Inspired by the portraits you take in the Camera app, Portrait mode in FaceTime blurs your background and puts the focus on you.",
        sup: 1,
      },
      {
        header: "Grid view        ",
        details:
          "Lets you see people in your Group FaceTime calls in the same-size tiles, and highlights the current speaker so it’s easy to know who’s talking. You’ll see up to six faces in the grid at a time.",
      },
      {
        header: "Spatial audio        ",
        details:
          "Creates a sound field that helps conversations flow as easily as they do face to face. Your friends’ voices are spread out to sound like they’re coming from the direction in which they’re positioned on the call.",
        sup: 1,
      },
      {
        header: "Voice Isolation mode        ",
        details:
          "This microphone mode spotlights your voice by using machine learning to identify ambient noises and block them out. So a leaf blower outside or a dog barking in the next room won’t interrupt your call.",
        sup: 1,
      },
      {
        header: "Wide Spectrum mode        ",
        details:
          "This microphone mode brings every single sound into your call. It’s ideal for when you’re taking music lessons or want your friend to hear everything that’s happening in the space you’re in.",
        sup: 1,
      },
      {
        header: "FaceTime links        ",
        details:
          "Invite your friends into a FaceTime call using a web link you can share anywhere.",
      },
      {
        header: "Join FaceTime on the web        ",
        details:
          "Invite anyone to join you in a FaceTime call, even friends who don’t have an Apple device.3 They can join you for one-on-one and Group FaceTime calls right from their browser instantly — no login necessary.",
      },
      {
        header: "Calendar integration        ",
        details:
          "Generate a web link for a FaceTime call while creating an event in Calendar, so everyone knows exactly where to meet and when.",
      },
      {
        header: "Mute alerts        ",
        details:
          "Lets you know when you’re talking while muted. Tap the alert to quickly unmute and make sure your voice is heard.",
      },
      {
        header: "Zoom        ",
        details:
          "An optical zoom control for your back camera helps you zero in on what matters when you’re on a FaceTime call.",
      },
    ],
  },
];
