import React from "react";
import "./header.css";
export const Header = () => {
  return (
    <div className="features-header">
      <h1>
        New features
        <br /> available with iOS 15.
      </h1>
      <p>
        iOS 15 brings amazing new features that help you connect, focus,
        explore, and do even more with iPhone.
      </p>
    </div>
  );
};
