import React from "react";
import { Links } from "../elements/links/links";
import "./navbar.css";
export class Navbar extends React.PureComponent {
  state = {
    showLinks: false,
  };
  handleClickedBtn = () => {
    let { showLinks } = this.state;
    let element = document.getElementById("full-screen");

    if (showLinks === false) {
      element.classList.add("full");
      element.classList.add("s");
    } else {
      element.classList.remove("full");
    }

    this.setState({ showLinks: !showLinks });
  };

  render() {
    let { showLinks } = this.state;
    console.log(showLinks);

    return (
      <div className="top-nav" style={{ posotion: "relative" }}>
        <nav
          className="navbar py-0 navbar navbar-expand-md  "
          id={showLinks ? "changed-background" : "default-background"}
        >
          <div className="container  ">
            <>
              <button
                className="navbar-toggler"
                type="button"
                onClick={this.handleClickedBtn}
              >
                {/* {!showLinks && (
                  <>
                    {" "} */}
                <span className="navbar-icon icon1"></span>
                <span className="navbar-icon icon2"></span>
                {/* </>
                )} */}
                {/* {showLinks && <i className="fa fa-close"></i>} */}
              </button>
              <Links className="py-0 brand" to="#">
                <div className="apple-icon"></div>
              </Links>
            </>{" "}
            <div
              className={`links ${showLinks ? "show" : "hide"}`}
              id="navbarCollapse"
            >
              <Links className="py-0    " to="#">
                <div className="mac"></div>
              </Links>
              <Links className="py-0    " to="#">
                <div className="ipad"></div>
              </Links>
              <Links className="py-0  " to="#">
                <div className="iphone"></div>
              </Links>
              <Links className="py-0   " to="#">
                <div className="watch"></div>
              </Links>
              <Links className="py-0   " to="#">
                <div className="airpods"></div>
              </Links>
              <Links className="py-0   " to="#">
                <div className="tv-home"></div>
              </Links>
              <Links className="py-0  " to="#">
                <div className="on-apple"></div>
              </Links>
              <Links className="py-0 " to="#">
                <div className="support"></div>
              </Links>{" "}
              <Links className="py-0 " to="# " style={{ borderBottom: 0 }}>
                <div className="where-to-buy"></div>
              </Links>{" "}
              <Links className="py-0    " to="#">
                <div className="search"></div>
              </Links>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
