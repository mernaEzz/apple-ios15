import React from "react";
import { Link } from "react-router-dom";
export const Links = (props) => {
  return <Link {...props}>{props.children}</Link>;
};
