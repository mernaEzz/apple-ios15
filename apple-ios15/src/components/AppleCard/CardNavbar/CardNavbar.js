import React from "react";
import { Links } from "../../elements/links/links";
import "./CardNavbar.css";
export class CardNavbar extends React.PureComponent {
  state = {
    expand: false,
  };
  handleExpandedMEnu = () => {
    let { expand } = this.state;
    this.setState({ expand: !expand });
  };
  render() {
    let { expand } = this.state;
    return (
      <div
        className="card-navbar"
        id={expand ? "expanded-menu" : "collapsed-menu"}
      >
        <div className="container   p-2">
          <h5 className="my-0 mr-md-auto ">Apple Card</h5>
          <nav className="my-2 my-md-0 mr-md-2">
            <Links className="p-2" value="overview" to="#">
              Overview
            </Links>{" "}
            <Links className="p-2" to="/apple-card/features">
              Features
            </Links>
            <Links className="p-2  " to="/apple-card/family">
              Family
            </Links>
            <Links className="p-2  " to="/apple-card/monthly-">
              Monthly Installments
            </Links>
            <Links className="p-2  " to="#">
              How-To videos
            </Links>
          </nav>
          <div className="row">
            {" "}
            <i
              className="fa fa-angle-down"
              onClick={this.handleExpandedMEnu}
              style={{ transform: expand ? "rotate(180deg)" : "rotate(0deg)" }}
            ></i>
            <Links className="btn btn-outline-primary" href="#">
              Apply now
            </Links>
          </div>
        </div>
      </div>
    );
  }
}
