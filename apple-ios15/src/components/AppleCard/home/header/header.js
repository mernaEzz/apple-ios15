import React from "react";
import headerVideo from "../../../../assets/videos/overview-header-video.mp4";
import "./header.css";
export const Header = () => {
  return (
    <div className="overview-header">
      <video src={headerVideo} />
    </div>
  );
};
