import React from "react";
import { Navbar } from "../../navbar/navbar";
import { CardNavbar } from "../CardNavbar/CardNavbar";
import { Header } from "./header/header";
import "./home.css";
export const CardHome = () => {
  return (
    <div className="apple-card-home">
      <Navbar />
      <CardNavbar />
      <Header />
    </div>
  );
};
