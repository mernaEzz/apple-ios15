import React from "react";
import { Links } from "../elements/links/links";
import "./ios13Nav.css";
export const Ios13Nav = (props) => {
  return (
    <div className={`ios13-nav ${props.scroll ? "is-scroll" : ""} `}>
      <div className="container">
        <Links to="/apple-ios13">iOS 13</Links>
      </div>
    </div>
  );
};
