import React from "react";
import { LeftScreens } from "./leftScreens/leftScreens";
import { MainScreen } from "./mainScreen/mainScreen";
import { RightScreens } from "./rightScreens/rightScreens";
import "./header.css";
export class Header extends React.PureComponent {
  state = {
    scale: window.innerWidth / 222,
    headerPosition: "sticky",
    transformed: "transformed",
    finalRes: window.innerWidth / 222,
  };
  componentDidMount() {
    // this.handleScreenResize();
    window.addEventListener("scroll", () => {
      this.handleScreenScroll(window.scrollY / 650);
    });
    window.addEventListener("resize", () => {
      this.handleScreenScroll(-window.innerWidth / 11100);
      this.handleScreenScroll(-window.innerHeight / 11100);
    });
  }
  handleScreenScroll = (scrollFromTop) => {
    let { finalRes, scale, headerPosition, transformed } = this.state;
    var finalScale = 1.2;
    var diffWidth = scale - finalScale;
    // scrollFromTop = window.scrollY;
    finalRes = scale - diffWidth * scrollFromTop;
    if (finalRes <= 0.8) {
      finalRes = 0.8;
    }
    if (window.scrollY <= 0) {
      transformed = "transformed";
    } else {
      transformed = "";
    }
    console.log(window.scrollX);
    this.setState({ scale, headerPosition, transformed, finalRes });
  };
  handleScreenResize = () => {
    let { finalRes, scale } = this.state;
    var finalScale = 1.2;
    var diffWidth = scale - finalScale;
    var scrollFromTop = -window.innerWidth;
    finalRes = scale - diffWidth * (scrollFromTop / 20000);
    if (finalRes <= 0.8) {
      finalRes = 0.8;
    }
    this.setState({ scale, finalRes });
  };
  render() {
    let { finalRes, headerPosition, transformed } = this.state;

    return (
      <div style={{ position: "relative", height: "2480px" }}>
        <div className="home-header" style={{ position: headerPosition }}>
          <LeftScreens />
          <MainScreen finalRes={finalRes} transformed={transformed} />
          <RightScreens />
        </div>
      </div>
    );
  }
}
