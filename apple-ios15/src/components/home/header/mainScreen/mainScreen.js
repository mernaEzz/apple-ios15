import React from "react";
import "./mainScreen.css";
export class MainScreen extends React.PureComponent {
  render() {
    let { finalRes, transformed } = this.props;
    return (
      <div
        className={`main-screen ${transformed}`}
        style={{
          transform: "matrix(" + finalRes + ", 0, 0, " + finalRes + ", 0, 0)",
        }}
      >
        <figure className="screen" />
        <figure className="hardware" />
      </div>
    );
  }
}
