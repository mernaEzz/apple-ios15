import React from "react";
import { Ios13Nav } from "../ios13Nav/ios13Nav";
import { Navbar } from "../navbar/navbar";
import { Header } from "./header/header";
export class Home extends React.PureComponent {
  state = {
    scroll: false,
    count: -500,
  };
  componentDidMount() {
    window.onscroll = () => {
      if (window.scrollY >= 50) {
        this.handlePageScroll();
      } else {
        this.setState({
          scroll: false,
        });
      }
    };
    setInterval(() => {
      let { count } = this.state;
      this.setState({ count: count + 100 });
      if (count >= 0) {
        this.setState({ count: 0 });
      }
    }, 200);
  }

  handlePageScroll = () => {
    this.setState({
      scroll: true,
    });
  };
  render() {
    return (
      <div style={{ height: "2000px" }}>
        <Navbar /> {this.state.count}
        <Ios13Nav scroll={this.state.scroll} />
        <main style={{ height: "inherit" }}>
          <Header />
        </main>
      </div>
    );
  }
}
